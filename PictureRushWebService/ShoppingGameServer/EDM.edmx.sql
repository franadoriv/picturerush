
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 08/21/2015 15:39:40
-- Generated from EDMX file: C:\Users\VP46327\Desktop\Personal Projects\ShoppingGame\ShoppingGameServer\ShoppingGameServer\EDM.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;

USE [ShoppingGame];

IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');


-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[sg].[FK_T_Picture_T_PictureGroup]', 'F') IS NOT NULL
    ALTER TABLE [sg].[T_Picture] DROP CONSTRAINT [FK_T_Picture_T_PictureGroup];

IF OBJECT_ID(N'[sg].[FK_T_Picture_T_UserAccount]', 'F') IS NOT NULL
    ALTER TABLE [sg].[T_Picture] DROP CONSTRAINT [FK_T_Picture_T_UserAccount];


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[sg].[T_Picture]', 'U') IS NOT NULL
    DROP TABLE [sg].[T_Picture];

IF OBJECT_ID(N'[sg].[T_PictureGroup]', 'U') IS NOT NULL
    DROP TABLE [sg].[T_PictureGroup];

IF OBJECT_ID(N'[sg].[T_UserAccount]', 'U') IS NOT NULL
    DROP TABLE [sg].[T_UserAccount];


-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'T_Picture'
CREATE TABLE [dbo].[T_Picture] (
    [pictureid] int IDENTITY(1,1) NOT NULL,
    [groupid] int  NOT NULL,
    [userid] int  NOT NULL,
    [picturename] nvarchar(50)  NOT NULL,
    [filename] nvarchar(100)  NOT NULL,
    [creationdate] datetime  NOT NULL,
    [includeongame] bit  NULL
);


-- Creating table 'T_PictureGroup'
CREATE TABLE [dbo].[T_PictureGroup] (
    [groupid] int IDENTITY(1,1) NOT NULL,
    [groupname] nvarchar(100)  NULL
);


-- Creating table 'T_UserAccount'
CREATE TABLE [dbo].[T_UserAccount] (
    [userid] int IDENTITY(1,1) NOT NULL,
    [facebookid] nvarchar(50)  NULL,
    [twitterid] nvarchar(50)  NULL,
    [email] nvarchar(50)  NULL,
    [password] nvarchar(50)  NULL,
    [name] nvarchar(50)  NULL,
    [lastname] nvarchar(50)  NULL,
    [creationdate] datetime  NULL,
    [lastlogin] datetime  NULL,
    [ogleid] nvarchar(50)  NULL
);


-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [pictureid] in table 'T_Picture'
ALTER TABLE [dbo].[T_Picture]
ADD CONSTRAINT [PK_T_Picture]
    PRIMARY KEY CLUSTERED ([pictureid] ASC);


-- Creating primary key on [groupid] in table 'T_PictureGroup'
ALTER TABLE [dbo].[T_PictureGroup]
ADD CONSTRAINT [PK_T_PictureGroup]
    PRIMARY KEY CLUSTERED ([groupid] ASC);


-- Creating primary key on [userid] in table 'T_UserAccount'
ALTER TABLE [dbo].[T_UserAccount]
ADD CONSTRAINT [PK_T_UserAccount]
    PRIMARY KEY CLUSTERED ([userid] ASC);


-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [groupid] in table 'T_Picture'
ALTER TABLE [dbo].[T_Picture]
ADD CONSTRAINT [FK_T_Picture_T_PictureGroup]
    FOREIGN KEY ([groupid])
    REFERENCES [dbo].[T_PictureGroup]
        ([groupid])
    ON DELETE NO ACTION ON UPDATE NO ACTION;


-- Creating non-clustered index for FOREIGN KEY 'FK_T_Picture_T_PictureGroup'
CREATE INDEX [IX_FK_T_Picture_T_PictureGroup]
ON [dbo].[T_Picture]
    ([groupid]);


-- Creating foreign key on [userid] in table 'T_Picture'
ALTER TABLE [dbo].[T_Picture]
ADD CONSTRAINT [FK_T_Picture_T_UserAccount]
    FOREIGN KEY ([userid])
    REFERENCES [dbo].[T_UserAccount]
        ([userid])
    ON DELETE NO ACTION ON UPDATE NO ACTION;


-- Creating non-clustered index for FOREIGN KEY 'FK_T_Picture_T_UserAccount'
CREATE INDEX [IX_FK_T_Picture_T_UserAccount]
ON [dbo].[T_Picture]
    ([userid]);


-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------