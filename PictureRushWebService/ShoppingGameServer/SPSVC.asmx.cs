﻿using ShoppingGameServer.Helpers;
using ShoppingGameServer.Interfaces;
using ShoppingGameServer.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace ShoppingGameServer {
    /// <summary>
    /// Descripción breve de Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    [System.Web.Script.Services.ScriptService]
    public class SPSVC : System.Web.Services.WebService {

        [WebMethod]
        public LoginResponse DoLogin(string EMail, string Password, string FBID, string TWID,string GOID,string Name,string LastName) {
            return ILogin._DoLogin(EMail,Password, FBID, TWID, GOID,Name,LastName);
        }

        [WebMethod]
        public BasicResponse AccountCreate(string EMail, string Password, string Name, string LastName) {
            return ILogin._AccountCreate(EMail, Password,Name,LastName );
        }

        [WebMethod]
        public PictureGroupsResponse GetPictureGroups(int UserID) {
            return IPicture._GetPictureGroups(UserID);
        }

        [WebMethod]
        public UserPicturesResponse GetUserPictures(int UserID, bool IncludeImageData, bool OnlySelected) {
            return IPicture._GetUserPictures(UserID, IncludeImageData, OnlySelected);
        }

        [WebMethod]
        public BasicResponse SavePicture(int UserID,int GroupID, string ImageName, string Base64) {
            return IPicture._SavePicture(UserID, GroupID, ImageName, Base64);
        }

        [WebMethod]
        public BasicResponse SelectPictures(int UserID,string PictureIDs) {
            List<int> IDS = new List<int>();
            foreach (string S in PictureIDs.Split(',')) {
                IDS.Add(Convert.ToInt32(S));
            }
            PictureIDs.Split(',');
            return IPicture._SelectPictures(UserID, IDS.ToArray());
        }

        [WebMethod]
        public BasicResponse DeletePicture(int PictureID) {
            return IPicture._DeletePicture(PictureID);
        }

        [WebMethod]
        public void ShowPicture(int PictureID) {
            PictureHelper.GetPictureData(PictureID);
        }

        [WebMethod]
        public BasicResponse SetPicturesStatus(string JsonPicturesStatus) {
            return IPicture._SetPicturesStatus(JsonPicturesStatus);
        }
    }
}