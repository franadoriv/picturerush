﻿using ShoppingGameServer.Helpers;
using ShoppingGameServer.Objects;
using ShoppingGameServer.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace ShoppingGameServer.Interfaces {
    public class IPicture {
        public static BasicResponse _SavePicture(int UserID, int GroupID, string ImageName, string Base64) {
            var R = new BasicResponse();
            try {
                PictureHelper.SavePicture(UserID, GroupID, ImageName, Base64);
                R.Result = "OK";
            }
            catch (Exception e){
                R.ErrMess = e.Message;
                R.StackTrace = e.StackTrace;
            }
            return R;
        }

        public static BasicResponse _DeletePicture(int PictureID) {
            var R = new BasicResponse();
            try {
                PictureHelper.DeletePicture(PictureID);
                R.Result = "OK";
            }
            catch (Exception e) {
                R.ErrMess = e.Message;
                R.StackTrace = e.StackTrace;
            }
            return R;
        }

        public static PictureGroupsResponse _GetPictureGroups(int UserID) {
            var R = new PictureGroupsResponse();
            try {
                R.PictureGroups = PictureHelper.GetPictureGroups(UserID);
                R.Result = "OK";
            }
            catch (Exception e) {
                R.ErrMess = e.Message;
                R.StackTrace = e.StackTrace;
            }
            return R;
        }

        public static UserPicturesResponse _GetUserPictures(int UserID, bool IncludeImageData, bool OnlySelected) {
            var R = new UserPicturesResponse();
            try {
                R.Pictures = PictureHelper.GetUserPictures(UserID, IncludeImageData, OnlySelected);
                R.Result = "OK";
            }
            catch (Exception e) {
                R.ErrMess = e.Message;
                R.StackTrace = e.StackTrace;
            }
            return R;
        }

        public static BasicResponse _SelectPictures(int UserID,int[] PictureIDs) {
            var R = new BasicResponse();
            try {
                PictureHelper.SelectPictures(UserID,PictureIDs);
                R.Result = "OK";
            }
            catch (Exception e) {
                R.ErrMess = e.Message;
                R.StackTrace = e.StackTrace;
            }
            return R;
        }
        public static BasicResponse _SetPicturesStatus(string JsonPicturesStatus) {
            var R = new BasicResponse();
            try {
                var PicturesStatus = new JavaScriptSerializer().Deserialize<List<objPictureStatus>>(JsonPicturesStatus);
                PictureHelper.SetPicturesStatus(PicturesStatus);
                R.Result = "OK";
            }
            catch (Exception e) {
                R.ErrMess = e.Message;
                R.StackTrace = e.StackTrace;
            }
            return R;
        }
        
    }
}