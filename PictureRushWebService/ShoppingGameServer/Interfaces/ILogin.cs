﻿using ShoppingGameServer.Helpers;
using ShoppingGameServer.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShoppingGameServer.Interfaces {
    class ILogin {

        public static LoginResponse _DoLogin(string EMail, string Password, string FBID, string TWID,string GOID,string Name = "", string LastName = "") {
            LoginResponse R = new LoginResponse();
            try {
                R.UserAccount = LoginHelper.Login(EMail, Password, FBID, TWID, GOID, Name,LastName);
                R.Result = "OK";
            }
            catch (Exception e) {
                R.ErrMess = e.Message;
                R.StackTrace = e.StackTrace;
                if (e.InnerException != null) {
                    R.InnerErrMess = e.InnerException.Message;
                    R.InnerStackTrace = e.InnerException.StackTrace;
                }
            }
            return R;
        }

        public static BasicResponse _AccountCreate(string EMail, string Password, string Name, string LastName) {
            BasicResponse R = new BasicResponse();
            try {
                LoginHelper.AccountCreate(EMail, Password, Name,LastName);
                R.Result = "OK";
            }
            catch (Exception e) {
                R.ErrMess = e.Message;
                R.StackTrace = e.StackTrace;
                if (e.InnerException != null) {
                    R.InnerErrMess = e.InnerException.Message;
                    R.InnerStackTrace = e.InnerException.StackTrace;
                }

            }
            return R;
        }

    }
}
