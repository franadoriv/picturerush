﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShoppingGameServer.Responses {
   public  class BasicResponse {
       public string ErrMess { get; set; }
        public string StackTrace { get; set; }
        public string Result { get; set; }
        public string InnerErrMess {get; set;}
        public string InnerStackTrace { get; set; }



    }
}
