﻿using ShoppingGameServer.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShoppingGameServer.Responses {
    public class UserPicturesResponse : BasicResponse {
        public List<objPicture> Pictures = new List<objPicture>();
    }
}