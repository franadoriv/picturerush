﻿using ShoppingGameServer.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShoppingGameServer.Responses {
    public class PictureGroupsResponse : BasicResponse {
        public List<objPictureGroup> PictureGroups = new List<objPictureGroup>();
    }
}
