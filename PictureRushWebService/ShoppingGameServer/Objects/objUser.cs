﻿using ShoppingGameServer.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShoppingGameServer.Objects {
    public class objUser {
        public string Name;
        public string LastName;
        public int UserID;
        public string EMail;
        public List<objPicture> Images = new List<objPicture>();

        public objUser() { }

        public objUser(T_UserAccount UA) {
            
            this.Name = UA.name;
            this.LastName = UA.lastname;
            this.UserID = UA.userid;
            this.EMail = UA.email;

            foreach (T_Picture Picture in UA.T_Picture) {
                this.Images.Add(new objPicture(Picture));
            }
        }

    }
}
