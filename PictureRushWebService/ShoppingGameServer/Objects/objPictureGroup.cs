﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShoppingGameServer.Objects {
    public class objPictureGroup {
        public int PictureGroupID;
        public string PictureGroupName;
        public List<objPicture> Pictures = new List<objPicture>(); 
        public objPictureGroup() { }

        public objPictureGroup(int PictureGroupID, string PictureGroupName) {
            this.PictureGroupID = PictureGroupID;
            this.PictureGroupName = PictureGroupName;
        }

        public objPictureGroup(T_PictureGroup TPG, int UserID) {
            DBSG DB = new DBSG();
            this.PictureGroupID = TPG.groupid;
            this.PictureGroupName = TPG.groupname;
            foreach (T_Picture TP in DB.T_Picture.Where(P => P.groupid == TPG.groupid && P.userid == UserID)) {
                this.Pictures.Add(new objPicture(TP));
            }
        }
    }
}
