﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShoppingGameServer.Objects {
    public class objPictureStatus {
        public int PictureID {get; set;}
        
        public bool Active {get; set;}

        public objPictureStatus() { }

        public objPictureStatus(int PictureID, bool Active) { this.PictureID = PictureID; this.Active = Active; }
    }
}