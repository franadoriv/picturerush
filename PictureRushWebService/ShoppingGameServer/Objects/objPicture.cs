﻿using ShoppingGameServer.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace ShoppingGameServer.Objects {
    public class objPicture {
        public int PictureID;
        public string PictureName;
        public string Base64Data;
        public DateTime DateStored;
        public bool IncludeOnGame = false;
        public objPicture() { }

        public objPicture(T_Picture Picture) {
            this.PictureID = Picture.pictureid; 
            this.PictureName = Picture.picturename;
            string Path = HttpContext.Current.Server.MapPath("~/Pictures/") + Picture.filename + ".png";
            //this.Base64Data = PictureHelper.GetBase64String(Path);
            this.DateStored = Picture.creationdate;
            this.IncludeOnGame = (bool)(Picture.includeongame??false);
        }

        public objPicture(int PictureID,string PictureName ,string Base64Data, DateTime DateStored,bool IncludeOnGame) {
            this.PictureID = PictureID; this.PictureName = PictureName; this.Base64Data = Base64Data; this.DateStored = DateStored;
            this.IncludeOnGame = IncludeOnGame;
        }
    }
}
