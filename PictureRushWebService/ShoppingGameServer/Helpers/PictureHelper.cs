﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Drawing;
using System.Data.Entity;
using ShoppingGameServer.Objects;
using System.Drawing.Imaging;


namespace ShoppingGameServer.Helpers {
    public class PictureHelper {

        public static void SavePicture(int UserID, int GroupID, string ImageName, string Base64) {
            byte[] data = Convert.FromBase64String(Base64);
            MemoryStream ms = new MemoryStream(data);
            Image img = Image.FromStream(ms);
            string FileName = System.IO.Path.GetRandomFileName().Split('.')[0];
            string Path = HttpContext.Current.Server.MapPath("~/Pictures/") + FileName + ".png";
            img.Save(Path);
            var DB = new DBSG();
            var Picture = new T_Picture();
            Picture.filename = FileName;
            Picture.picturename = ImageName;
            Picture.T_PictureGroup = DB.T_PictureGroup.Where(IG => IG.groupid == GroupID).FirstOrDefault();
            Picture.T_UserAccount = DB.T_UserAccount.Where(UA => UA.userid == UserID).FirstOrDefault();
            if (Picture.T_PictureGroup == null)
                throw new Exception("PictureGroup not found");
            if (Picture.T_UserAccount == null)
                throw new Exception("User not found");
            Picture.creationdate = DateTime.Now;
            DB.T_Picture.Add(Picture);
            DB.SaveChanges();
        }

        public static void DeletePicture(int PictureID) {
            var DB = new DBSG();
            var Pic = DB.T_Picture.Where(P => P.pictureid == PictureID).FirstOrDefault();
            if (Pic != null) {
                DB.T_Picture.Remove(Pic);
                DB.SaveChanges();
            }
        }

        public static string GetBase64String(string PathName) {
            Image img = Image.FromFile(PathName);
            MemoryStream ms = new MemoryStream();
            img.Save(ms, img.RawFormat);
            byte[] imageBytes = ms.ToArray();
            string base64String = Convert.ToBase64String(imageBytes);
            return base64String;
        }

        public static string GetPicture(int PictureID) {
            var DB = new DBSG();
            var T_IMG = DB.T_Picture.Where(P => P.pictureid == PictureID).FirstOrDefault();
            if (T_IMG == null)
                throw new Exception("Picture ID " + PictureID + " not found");
            string Path = HttpContext.Current.Server.MapPath("~/Pictures/") + T_IMG.filename + ".png";
            return GetBase64String(Path);
        }

        public static void GetPictureData(int PictureID) {
             var DB = new DBSG();
            var T_IMG = DB.T_Picture.Where(P => P.pictureid == PictureID).FirstOrDefault();
            if (T_IMG == null)
                throw new Exception("Picture ID " + PictureID + " not found");
            string Path = HttpContext.Current.Server.MapPath("~/Pictures/") + T_IMG.filename + ".png";
            Image image = Image.FromFile(Path);
            MemoryStream ms = new MemoryStream();
            image.Save(ms,ImageFormat.Png);
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ContentType = "image/png";
            ms.WriteTo(HttpContext.Current.Response.OutputStream);
            HttpContext.Current.Response.End();
        }

        public static List<objPicture> GetUserPictures(int UserID,bool IncludeImageData=false,bool OnlySelected = false) { 
            List<objPicture> R = new List<objPicture>();
            var DB = new DBSG();
            var UserPictures = DB.T_UserAccount.Where(UA => UA.userid == UserID).FirstOrDefault().T_Picture;
            var FilteredUserPictures = OnlySelected?UserPictures.Where(TP => TP.includeongame == OnlySelected):UserPictures;
            foreach (T_Picture T_IMG in FilteredUserPictures) {
                string Path = HttpContext.Current.Server.MapPath("~/Pictures/") + T_IMG.filename + ".png";
                R.Add(
                    new objPicture(
                        T_IMG.pictureid, 
                        T_IMG.picturename, 
                        IncludeImageData?GetBase64String(Path):"", 
                        T_IMG.creationdate,
                        T_IMG.includeongame??false));
            }
            return R;
        }

        public static List<objPictureGroup> GetPictureGroups(int UserID) {
            var R = new List<objPictureGroup>();
            var DB = new DBSG();
            foreach (T_PictureGroup TPG in DB.T_PictureGroup) {
                R.Add(new objPictureGroup(TPG, UserID));
            }
            return R;
        }

        public static void SelectPictures(int UserID,int[] PictureIDs) {
            var DB = new DBSG();
            var UserAccount = DB.T_UserAccount.Where(UA => UA.userid == UserID).FirstOrDefault();
            if (UserAccount == null)
                throw new Exception("User ID " + UserID + " not found");
            foreach (var P in UserAccount.T_Picture) {
                P.includeongame = false;
            }
            foreach(var P in DB.T_Picture.Where(TP=>PictureIDs.Any(PID=>PID ==TP.pictureid))){
                P.includeongame = true;
            }
            DB.SaveChanges();
        }

        public static void SetPicturesStatus(List<objPictureStatus> PicturesStatus) {
            var DB = new DBSG();
            foreach (var PS in PicturesStatus) {
                var PT = DB.T_Picture.Where(P => P.pictureid == PS.PictureID).FirstOrDefault();
                if (PT != null)
                    PT.includeongame = PS.Active;
            }
            DB.SaveChanges();
        }
    
    }
}