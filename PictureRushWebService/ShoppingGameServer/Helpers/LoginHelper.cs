﻿using ShoppingGameServer.Objects;
using ShoppingGameServer.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShoppingGameServer.Helpers {
    public class LoginHelper {

        public static objUser Login(string EMail, string Password, string FBID, string TWID,string GOID,string Name = "",string LastName = "") {
            var DB = new DBSG();
            T_UserAccount TUserAccount = null;
            if (!string.IsNullOrEmpty(EMail) && !string.IsNullOrEmpty(Password)) {
                if (DB.T_UserAccount.Where(UA => UA.email == EMail).FirstOrDefault() == null) {
                   //throw new Exception("Account not found");
                    int NewAccountID = AccountCreate(EMail, Password, Name, LastName);
                    TUserAccount = DB.T_UserAccount.Where(UA => UA.userid == NewAccountID).FirstOrDefault();
                    if (TUserAccount == null)
                        throw new Exception("Account not found after creation");
                }
                else if ((TUserAccount = DB.T_UserAccount.Where(UA => UA.email == EMail && UA.password == Password).FirstOrDefault()) == null)
                    throw new Exception("Wrong password");
            }
            else if ((!string.IsNullOrEmpty(EMail)) && (!string.IsNullOrEmpty(FBID) || !string.IsNullOrEmpty(TWID) || !string.IsNullOrEmpty(GOID))) {
                TUserAccount = DB.T_UserAccount.Where(UA => (UA.email == EMail) && (UA.facebookid == FBID || UA.twitterid == TWID || UA.googleid == GOID)).FirstOrDefault();
                if (TUserAccount == null) {
                    TUserAccount = new T_UserAccount();
                    if(!string.IsNullOrEmpty(FBID))
                        TUserAccount.facebookid = FBID;
                    if(!string.IsNullOrEmpty(TWID))
                        TUserAccount.twitterid = TWID;
                    if (!string.IsNullOrEmpty(TWID))
                        TUserAccount.twitterid = TWID;
                    if (!string.IsNullOrEmpty(GOID))
                        TUserAccount.googleid = GOID;
                    if (!string.IsNullOrEmpty(Name))
                        TUserAccount.name = Name;
                    if (!string.IsNullOrEmpty(LastName))
                        TUserAccount.lastname = LastName;
                    TUserAccount.creationdate = DateTime.Now;
                    DB.T_UserAccount.Add(TUserAccount);
                }
            }
            else
                throw new Exception("Incomplete parameters");
            TUserAccount.lastlogin = DateTime.Now;
            if (!string.IsNullOrEmpty(EMail))
                TUserAccount.email = EMail;
            DB.SaveChanges();
            return new objUser(TUserAccount);
        }

        public static int AccountCreate(string EMail, string Password,string Name,string LastName) {
            var DB = new DBSG();
            T_UserAccount TUser = null;
            if ((TUser=DB.T_UserAccount.Where(UA => UA.email == EMail).FirstOrDefault()) != null)
                throw new Exception("Account already exist's");
            TUser = new T_UserAccount();
            TUser.email = EMail;
            TUser.password = Password;
            TUser.creationdate = DateTime.Now;
            TUser.lastlogin = DateTime.Now;
            DB.T_UserAccount.Add(TUser);
            DB.SaveChanges();
            return TUser.userid;
        }




    }
}
