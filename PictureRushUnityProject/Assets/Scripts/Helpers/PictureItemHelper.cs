﻿using UnityEngine;
using System.Collections;

public class PictureItemHelper : MonoBehaviour {

    public UITexture PictureTexture;
    public int PictureID;
    ServiceManager SVCM;
    MainSceneCode MSC;

    public void LoadPicture(objPicture P) {
        this.PictureID = P.PictureID;
        StartCoroutine(SVCM.GetPicture(PictureID, (T) => {
            if (PictureTexture != null)
                PictureTexture.mainTexture = T;
            var TW = PictureTexture.GetComponentInChildren<UITweener>();
            var CHK = GetComponentInChildren<UIToggle>();
            Debug.Log("Setting check to " + P.UseThisPicture + " on PID " + P.PictureID + " on OBJ: " + CHK.name);
            GetComponentInChildren<UIToggle>().value = P.UseThisPicture; 
            TW.enabled = false;
            Destroy(TW);
            MSC.GM.DelayFrameAction(1, () => {
                var LEA = PictureTexture.transform.localEulerAngles;
                LEA = new Vector3(0,0,0);
                PictureTexture.transform.localEulerAngles = LEA; 
            });
        }));
        
    }


	// Use this for initialization
	void Awake () {
        SVCM = GameObject.Find("ServiceManager").GetComponent<ServiceManager>();
        MSC = GameObject.Find("MainSceneCode").GetComponent<MainSceneCode>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
