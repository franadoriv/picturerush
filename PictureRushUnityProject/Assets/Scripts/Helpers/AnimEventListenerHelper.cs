﻿using UnityEngine;
using System.Collections;
using System;

public class AnimEventListenerHelper : MonoBehaviour {



    public Action OnDamageApply, OnAtackFinish, OnDyingFinish, OnDamageReceive;
    public Action<bool> OnAtackInterrupted;

    public void DamageApply() {
        Debug.Log("Damaged");
        if (OnDamageApply != null)
            OnDamageApply();
    }

    public void AtackInterrupted(bool Dead) {
        Debug.Log("AtackInterrupted");
        if (OnAtackInterrupted != null)
            OnAtackInterrupted(Dead);
    }

    public void DamageReceive() {
        Debug.Log("DamageReceive");
        if (OnDamageReceive != null)
            OnDamageReceive();
    }

    public void AtackFinish() {
        Debug.Log(gameObject.name + " AtackFinish");
        if (OnAtackFinish != null)
            OnAtackFinish();
    }

    public void DyingFinish() {
        Debug.Log(gameObject.name + " DyingFinish");
        if (OnDyingFinish != null)
            OnDyingFinish();
    }

    
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
