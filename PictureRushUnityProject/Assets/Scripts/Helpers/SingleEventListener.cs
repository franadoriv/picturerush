﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class SingleEventListener : MonoBehaviour {

    public GameObject[] GameObjectsPool;
   



    public void OnExecute(string TargetAndFunction) {
        string Target = TargetAndFunction.Split(';')[0];
        string Function = TargetAndFunction.Split(';')[1];
        GameObject GO = GameObject.Find(Target);
        if(GO!=null)
            GO.SendMessage(Function);
    }

    public void ActivateAnimatorTrigger(string TargetAndTrigger) {
        string Target = TargetAndTrigger.Split(';')[0];
        string Trigger = TargetAndTrigger.Split(';')[1];
        GameObject.Find(Target).GetComponent<Animator>().SetTrigger(Trigger);
    }
    /*
    public void PlaySFX(string SFXName) {
        //Debug.Log("Searching for play " + SFXName);
        AudioManager AM = GameObject.Find("AudioManager").GetComponent<AudioManager>();
        AudioClip AC = GameObject.Find("SoundLibraryHelper").GetComponent<SoundLibraryHelper>().Sounds.Where(W => W.name == SFXName).FirstOrDefault();
        //Debug.Log("AC: " + AC);
        AM.PlaySFX(AC);
    }

    public void PlaySFXOnAudioSource(string AudioSourceAndSFX) {
        //Debug.Log("Searching for play " + AudioSourceAndSFX.Split(';')[1] + " in " + AudioSourceAndSFX.Split(';')[0]);
        AudioSource AD = GameObject.Find(AudioSourceAndSFX.Split(';')[0]).GetComponent<AudioSource>();
        AudioClip AC = GameObject.Find("SoundLibraryHelper").GetComponent<SoundLibraryHelper>().Sounds.Where(W => W.name == AudioSourceAndSFX.Split(';')[1]).FirstOrDefault();
        //Debug.Log("AC: " + AC);
        AD.PlayOneShot(AC);
    }

    public void PlayBGM(string BGMName) {
        Debug.Log("Searching for play " + BGMName);
        AudioManager AM = GameObject.Find("AudioManager").GetComponent<AudioManager>();
        AudioClip AC = GameObject.Find("SoundLibraryHelper").GetComponent<SoundLibraryHelper>().Musics.Where(W => W.name == BGMName).FirstOrDefault();
        Debug.Log("AC: " + AC);
        AM.PlayBGM(AC,false);
    }*/

    public void CreateLocalParticle(string ParticleNameAndOrigin) {
        string ParticleName = ParticleNameAndOrigin.Split(';')[1];
        string ParticleOrigin = ParticleNameAndOrigin.Split(';')[0];
        //Debug.Log("Creating particle '" + ParticleName + "' on '" + ParticleOrigin + "' GameObject");
        var ParticleObj = GameObjectsPool.Where(P => P.name == ParticleName).FirstOrDefault();
        var ParticleOriginObj = GameObjectsPool.Where(P => P.name == ParticleOrigin).FirstOrDefault();
        var NewParticle = ((GameObject)Instantiate(ParticleObj, ParticleOriginObj.transform.position, ParticleOriginObj.transform.transform.rotation));
        NewParticle.SetActive(true);
        NewParticle.transform.parent = ParticleOriginObj.transform;
        Destroy(NewParticle, 1);
    }


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
