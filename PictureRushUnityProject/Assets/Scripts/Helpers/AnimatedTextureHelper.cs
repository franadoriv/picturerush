﻿using UnityEngine;
using System.Collections;

public class AnimatedTextureHelper : MonoBehaviour {

    public Texture TargetTexture;
    public UITexture TargetUITexture;
    public Texture[] ImageList;

    public float SecondsInterval = 1;
    int CurrentImageIndex = 0;
    IEnumerator MainCycle() {
        while (true) {
            if (ImageList == null) break;
            CurrentImageIndex = CurrentImageIndex >= ImageList.Length ? 0 : CurrentImageIndex;
            TargetTexture = ImageList[CurrentImageIndex];
            if (TargetUITexture != null)
                TargetUITexture.mainTexture = ImageList[CurrentImageIndex];
            CurrentImageIndex += 1;
            yield return new WaitForSeconds(SecondsInterval);
        }
    }

	// Use this for initialization
	void Start () {
        if (ImageList != null)
            if (ImageList.Length > 0)
                StartCoroutine(MainCycle());
	}
	
	// Update is called once per frame
	void Update () {

	
	}
}
