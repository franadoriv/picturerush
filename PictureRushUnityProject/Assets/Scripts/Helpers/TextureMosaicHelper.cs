﻿using UnityEngine;
using System.Collections;

public class TextureMosaicHelper : MonoBehaviour {
    public UITexture TextureA;
    public Texture TextureB;
    public Material TextureC;
    public Vector2 Velocity = new Vector2(1,1);
    public Vector2 CurrentPos = Vector2.zero;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void LateUpdate () {
        if (TextureA != null) {
            var UVR = TextureA.uvRect;
            UVR.x += Velocity.x;
            UVR.y += Velocity.y;
            TextureA.uvRect= UVR;
        }
        if (TextureB != null) {
   
        }
        if (TextureC != null) {
            TextureC.SetTextureOffset("_MainTex", CurrentPos);
        }
        CurrentPos += Velocity;
	}
}
