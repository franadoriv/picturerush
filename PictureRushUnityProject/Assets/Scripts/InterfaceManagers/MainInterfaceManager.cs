﻿using UnityEngine;
using System.Collections;
using System;
using System.Linq;
public class MainInterfaceManager : MonoBehaviour {

    //Managers
    public MainSceneCode MSC;

    //Functions

    public void ChangeToPanel(GameObject To) {
        GameObject Actual;
        var ActualT = MSC.GM.SearchTransforms(MSC.IM.transform, "PNL_", false, false).ToList().FirstOrDefault();
        Actual = ActualT != null ? ActualT.gameObject : null;
        ChangeToPanel(Actual, To, null); 
    }

    public void ChangeToPanel(GameObject To, Action OnFinish = null) {
        GameObject Actual;
        var ActualT = MSC.GM.SearchTransforms(MSC.IM.transform, "PNL_", false, false).ToList().FirstOrDefault();
        Actual = ActualT != null ? ActualT.gameObject : null;
        ChangeToPanel(Actual, To, OnFinish); 
    }

    public void ChangeToPanel(GameObject Actual, GameObject To, Action OnFinish = null) {
       var ToAnm = To.GetComponent<UITweener>();
       if (Actual != null) {
           var ActAnm = Actual.GetComponent<UITweener>();
           var ActOF = new EventDelegate(() => {
               Actual.SetActive(false);
               To.SetActive(true);
               if (OnFinish != null) {
                   var ToOF = new EventDelegate(() => {
                       OnFinish();
                   });
                   ToOF.oneShot = true;
                   ToAnm.onFinished.Add(ToOF);
                   ToAnm.PlayForward();
               }
               ToAnm.PlayForward();
           });
           ActOF.oneShot = true;
           ActAnm.onFinished.Add(ActOF);
           ActAnm.PlayReverse();
       }
       else{
           To.SetActive(true);
           if (OnFinish != null) {
               var ToOF = new EventDelegate(() => {
                   OnFinish();
               });
               ToOF.oneShot = true;
               ToAnm.onFinished.Add(ToOF);
               ToAnm.PlayForward();
           }
           ToAnm.PlayForward();
       }
    }


    public void ChangeToPanel2(GameObject From, GameObject To, Action OnFinish = null)
    {
        Debug.Log("ChangePannel from " + From + " to " + To);
        var T2 = To.GetComponent<UITweener>();
        if (From != null && To != null)
        {
            var T1 = From.GetComponent<UITweener>();
            var Z1 = new EventDelegate(() =>
            {
                From.gameObject.SetActive(false);
                To.gameObject.SetActive(true);
                if (OnFinish != null)
                {
                    var Z2 = new EventDelegate(() =>
                    {
                        OnFinish();
                    });
                    Z2.oneShot = true;
                    T2.AddOnFinished(Z2);
                }

                T2.PlayForward();
            });
            Z1.oneShot = true;
            T1.AddOnFinished(Z1);
            T1.PlayReverse();
        }
        else
        {
            To.gameObject.SetActive(true);
            if (OnFinish != null)
            {
                var Z1 = new EventDelegate(() =>
                {
                    OnFinish();
                });
                Z1.oneShot = true;
                T2.AddOnFinished(Z1);
            }
            T2.PlayForward();
        }
    }


    public void ChangeToPanelByName(string To,Action OnFinish=null) {
		MSC = GameObject.Find("MainSceneCode").GetComponent<MainSceneCode>();
		Debug.Log ("ChangeToPanelByName: " + To + " MSC: " + MSC + " MSC.GM: " + MSC.GM);
		MSC.GM.DelayFrameAction (1, () => {
            Debug.Log("0 <- This is a zero");
            Debug.Log("1: " + MSC);
            Debug.Log("2: " + MSC.GM);
            Debug.Log("3: " + MSC.MIM.transform);
            Debug.Log("4: " + MSC.GM.SearchTransform(MSC.MIM.transform, To));
            ChangeToPanel(MSC.GM.SearchTransform(MSC.MIM.transform, To).gameObject, OnFinish);
		});
        
    }


    public void ChangeToPanelByName2(string To,Action OnFinish=null) {
		MSC = GameObject.Find("MainSceneCode").GetComponent<MainSceneCode>();
        Debug.Log("Changing pannel to: " + To + " MSC:"+MSC);
        ChangeToPanel2(null, MSC.GM.SearchTransform(transform, To).gameObject,OnFinish);
    }

    public void ChangeToPanelByName2(string From,string To, Action OnFinish = null) {
        MSC = GameObject.Find("MainSceneCode").GetComponent<MainSceneCode>();
        Debug.Log("Changing pannel to: " + To + " MSC:" + MSC);
        ChangeToPanel2(MSC.GM.SearchTransform(transform, From).gameObject, MSC.GM.SearchTransform(transform, To).gameObject, OnFinish);
    }



    public void ClosePanel(GameObject Panel, Action OnFinish = null) {
        var T = Panel.GetComponent<UITweener>();
        T.onFinished.Clear();
        var Z = new EventDelegate(() => {
            Panel.gameObject.SetActive(false);
            if (OnFinish != null)
                OnFinish();
        });
        Z.oneShot = true;
        T.AddOnFinished(Z);
        T.PlayReverse();
    }

    public void ClosePanelByName(string PanelName, Action OnFinish = null){
        MSC = GameObject.Find("MainSceneCode").GetComponent<MainSceneCode>();
        Debug.Log("Closing panel name: " + PanelName + " MSC: " + MSC + " MSC.GM: " + MSC.GM);
        MSC.GM.DelayFrameAction(1, () =>{
            ClosePanel(MSC.GM.SearchTransform(transform, PanelName).gameObject, OnFinish);
        });
    }



    public void OpenPanel(GameObject Panel, Action OnFinish = null) {
        var T = Panel.GetComponent<UITweener>();
        T.onFinished.Clear();
        var Z = new EventDelegate(() => {
            if (OnFinish != null)
                OnFinish();
        });
        Z.oneShot = true;
        T.AddOnFinished(Z);
        Panel.gameObject.SetActive(true);
        T.PlayForward();
    }

    public void OpenPanelByName(string To, Action OnFinish = null) {
        Debug.Log("OpenPannelByName: " + To);
        MSC = GameObject.Find("MainSceneCode").GetComponent<MainSceneCode>();
        OpenPanel(MSC.GM.SearchTransform(transform, To).gameObject, OnFinish);
    }
	
    

    // Use this for initialization
	void Awake () {
        MSC = GameObject.Find("MainSceneCode").GetComponent<MainSceneCode>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
