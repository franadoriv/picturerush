﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MainSceneCode : MonoBehaviour {

    //Managers
    public GlobalManager GM;
    public ObjectPoolManager OPM;
    public AudioManager AM;
    public CustomSceneManager SM;
    public MainInputManager IM;
    public ExternalImageManager EIM;
    public InterfaceManager IFM;
    public UserManager UM;
    public AccountManager ACTM;
    public AlertManager ALTM;
    public ServiceManager SVCM;
    public FileBrowserManager FBM;
    public MainInterfaceManager MIM;
    public ConfigManager CM;



    //Musics
    public AudioClip BGM_Main;

    void StartComponents() {
        ALTM.CStart();
        SM.PrepareComponents();
        EIM.MSC = this;
    }

    public void PrepareComponents() {
        GlobalManager.CheckGlobals();
        Debug.Log("GlobalManager");
        GM = GameObject.Find("GlobalManager").GetComponent<GlobalManager>();
        //OPM = GameObject.Find("ObjectPoolManager").GetComponent<ObjectPoolManager>();
        AM = GameObject.Find("AudioManager").GetComponent<AudioManager>();
        SM = GameObject.Find("SceneManager").GetComponent<CustomSceneManager>();
        IM = GameObject.Find("MainInputManager").GetComponent<MainInputManager>();
        EIM = GameObject.Find("ExternalImageManager").GetComponent<ExternalImageManager>();
        //IFM = GameObject.Find("V").GetComponent<MainInputManager>();
        UM = GameObject.Find("UserManager").GetComponent<UserManager>();
        ACTM = GameObject.Find("AccountManager").GetComponent<AccountManager>();
        ALTM = GameObject.Find("CNL_Alert").GetComponent<AlertManager>();
        SVCM = GameObject.Find("ServiceManager").GetComponent<ServiceManager>();
        FBM = GameObject.Find("FileBrowserManager").GetComponent<FileBrowserManager>();
        MIM = GameObject.Find("MainInterfaceManager").GetComponent<MainInterfaceManager>();
        CM = GameObject.Find("ConfigManager").GetComponent<ConfigManager>();
        StartComponents();
    }




}
