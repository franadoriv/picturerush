﻿using UnityEngine;
using System.Collections;
using System.Xml.Linq;
using System.Linq;

public class MSC_MainMenu : MainSceneCode {

    public AudioClip MainBGM;

    void Awake() {
        PrepareComponents();
    }

	// Use this for initialization
	void Start () {
        if (Application.isEditor) {
            GM.SearchTransform(transform, "TXT_EMail").GetComponent<UIInput>().value = "franadoriv@gmail.com";
            GM.SearchTransform(transform, "TXT_Password").GetComponent<UIInput>().value = "18363052";
        }
        AM.PlayBGM(MainBGM);
        GM.DelayAction(1, () => {
            SM.FadeIn(() => {
                CM.PassedThroughLogin = true;
                MIM.ChangeToPanelByName2(CM.PassedThroughLogin?"PNL_MainPanel":"PNL_Login");
            });
        });

	} 
	
	// Update is called once per frame
	void Update () {
	
	}
}
