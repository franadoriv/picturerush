﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
//using UnityEngine.Advertisements;
using UnityEngine.SceneManagement;
using GoogleMobileAds.Api;

public class MSC_Game : MainSceneCode {

    //GameObjects
    GameObject Courtain;
    GameObject Cell;
    GameObject Grid;
    GameObject Pivot;
    GameObject MasterPivot;
    GameObject PNL_TargetImage;
    Sprite SPR_Target;
    GameObject LBL_Score;
    GameObject LBL_Timer;
    GameObject LBL_Start;
    GameObject LBL_Speed;
    GameObject LastCellPressed;

    public GameObject PNL_TryAgain;
    public GameObject PNL_Continue;
    public GameObject LBL_TotalScore;
    public GameObject TEX_Fade;
    public GameObject EnviromentContent;
    public GameObject PNL_CountDownBonus;
    public GameObject LBL_CountDownBonus;

    //ADS
    InterstitialAd AdMobInterstitial;

    //Sounds
    public AudioClip SFX_GoodAnswer;
    public AudioClip SFX_BadAnswer;
    public AudioClip SFX_IntroCreationCell;
    public AudioClip SFX_BTN_TryAgain;
    public AudioClip SFX_BTN_GoBack;
    public AudioClip SFX_Fail;
    public AudioClip SFX_Finish;
    
    //Background Musics
    public AudioClip BGM_GamePlay;
    public AudioClip BGM_GameResume;

    //Properties
    public int CurrentScore = 0;
    public float PivotOffset = 0.0F;
    public bool GameOver = false;
    public float SpeedAcum = 0.00F;
    public float CurrentSpeed = 0.00F;
    public bool InternetAvailable = false;
    public bool BonusContinueUsed = false;
    public bool BonusContinueShowing = false;

    //Flags
    public bool InputEnabled = false;
    Stopwatch stopwatch = new Stopwatch();
    public bool FirstTouched = false;

    //Calculated
    Vector3 CellSize = new Vector3();
    Vector2 ScreenSize;
    int LastRowTargetIndex = 0;
    float LastCorrectKeyVPos = 0;
    public RectTransform LastCorrectKeyPos = null;
    public float LastCorrectKeyPosY = 0.0F;
    public int CurrentGoodRowClick = 0;

    //Lists
    List<GameObject> BTNs_Clicked = new List<GameObject>();
    List<Vector3> KeyPositions = new List<Vector3>();
    List<TweenMovement> TweenMovements = new List<TweenMovement>();
    public Sprite[] DefaultImages;
    public Sprite[] CustomImages;
    
    //Clases
    class TweenMovement {
        public static List<TweenMovement> TweenMovements = new List<TweenMovement>();
        public GameObject Target;
        public Vector3 To = new Vector3();
        Action<GameObject> OnFinish;
        public float Speed = 5.0F;
        public bool Finished = false;


        public void Update() {
            if (Target != null) {
                var Current = Target.GetComponent<RectTransform>().localPosition;
                Target.GetComponent<RectTransform>().localPosition = iTween.Vector2Update(Current, To, Speed);
                if (Vector3.Distance(Current, To) < 0.01F) {
                    TweenMovements.Remove(this);
                    if (OnFinish != null)
                        OnFinish(Target);
                }

            }
        }


        public TweenMovement(GameObject Target, Vector2 To, Action<GameObject> OnFinish = null) {
            this.Target = Target;
            this.To = To;
            this.OnFinish = OnFinish;
        }

        public static void UpdateAll() {
            //int N = 0;
            for (var N = 0; N < TweenMovements.Count(); N++) 
                TweenMovements[N].Update();
        }

        public static void Create(GameObject Target, Vector2 To, Action<GameObject> OnFinish = null) {
            TweenMovements.Add(new TweenMovement(Target, To, OnFinish));
        }
    }

  
    //******************* Main Functions *************
    void AddRow(RectTransform BTN_RT) {
        //No tengo idea porque puse esta linea
        //if (LastCorrectKeyVPos == BTN_RT.localPosition.y) { Finish(true); return; }
        LastCorrectKeyVPos = BTN_RT.localPosition.y;
        var Cells = Grid.GetComponentsInChildren<Transform>().Where(C => C.name.Contains("Cell"));
        var RowPosCell = CM.GameOptions.Direction == ConfigManager.Options.GameDirection.Down?
                                                                            Cells.OrderByDescending(item => item.GetComponent<RectTransform>().position.y).Last():
                                                                            Cells.OrderByDescending(item => item.GetComponent<RectTransform>().position.y).First();
        var LastRowCells = Cells.Where(C => C.GetComponent<RectTransform>().position.y == RowPosCell.GetComponent<RectTransform>().position.y);
        
        //Obtenemos y destruimos la ultima fila
        foreach (Transform Cell in LastRowCells)
                GameObject.Destroy(Cell.gameObject);
        int CC = 0;

        //Creamos una nueva fila superior
        List<GameObject> RowCells = new List<GameObject>();
        for (CC = 0; CC < CM.GameOptions.ColCant; CC++) {
                var NewCell = GameObject.Instantiate(Cell);
                NewCell.transform.SetParent(Pivot.transform);
                NewCell.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
                NewCell.GetComponent<RectTransform>().sizeDelta = CellSize;
                //Esta linea es un gas, nisiquiera recuerdo que hace :(
                Vector3 Position;
                if (CM.GameOptions.Direction == ConfigManager.Options.GameDirection.Down)
                    Position = new Vector3(((-ScreenSize.x / 2) + CellSize.x / 2) + (CC * CellSize.x), ((ScreenSize.y / 2) - CellSize.y / 2) + (CurrentGoodRowClick * CellSize.y) + (CM.GameOptions.RowCant * CellSize.y), 0);
                else
                    Position = new Vector3(((-ScreenSize.x / 2) + CellSize.x / 2) + (CC * CellSize.x), (-(ScreenSize.y / 2) + CellSize.y / 2) - (CurrentGoodRowClick * CellSize.y) - (CM.GameOptions.RowCant * CellSize.y), 0);
                NewCell.GetComponent<RectTransform>().localPosition = Position;
                NewCell.name = "NewCell:"+NewCell.GetInstanceID();
                RowCells.Add(NewCell);
            }
            int RowTargetIndex = UnityEngine.Random.Range(0, RowCells.Count);
            while (LastRowTargetIndex == RowTargetIndex && CM.GameOptions.Difficulty != ConfigManager.Options.GameDifficulty.Easy) RowTargetIndex = UnityEngine.Random.Range(0, RowCells.Count);
            LastRowTargetIndex = RowTargetIndex;
            var TargetCell = RowCells[LastRowTargetIndex];
            SetCellSPR(TargetCell, SPR_Target);
            var OtherCells = RowCells.Where(OC => OC.GetInstanceID() != TargetCell.GetInstanceID());
            foreach (GameObject OtherCell in OtherCells) {
                var SPR = GetRandSPR();
                while (SPR == SPR_Target) SPR = GetRandSPR();
                SetCellSPR(OtherCell, SPR);
            }

    }

    void PrepareGrid(Action OnFinish = null) {
        int C = 0, R = 0;
        ScreenSize = Grid.GetComponent<RectTransform>().sizeDelta;
        CellSize = new Vector2(ScreenSize.x / CM.GameOptions.ColCant, ScreenSize.y / CM.GameOptions.RowCant);
        UnityEngine.Debug.Log("ScreenSize or GridSize: " + ScreenSize);
        UnityEngine.Debug.Log("CellSize: " + CellSize);
        for (R = -CM.GameOptions.RowCant; R < CM.GameOptions.RowCant * 2; R++) {
            List<GameObject> RowCells = new List<GameObject>();
            for (C = 0; C < CM.GameOptions.ColCant; C++) {
                var NewCell = GameObject.Instantiate(Cell);
                NewCell.SetActive(false);
                NewCell.transform.parent = Pivot.transform;
                NewCell.GetComponent<RectTransform>().localScale = new Vector3(1, 1,1);
                NewCell.GetComponent<RectTransform>().sizeDelta = CellSize;
                var Position = new Vector3(((-ScreenSize.x / 2) + CellSize.x / 2) + (C * CellSize.x), ((ScreenSize.y / 2) - CellSize.y / 2) - (R * CellSize.y), 0);
                if (R >= 0 && R < CM.GameOptions.RowCant)
                    KeyPositions.Add(Position);
                NewCell.GetComponent<RectTransform>().localPosition = Position;
                NewCell.name = "Cell:" + NewCell.GetInstanceID();
                RowCells.Add(NewCell);
            }
            int RowTargetIndex = UnityEngine.Random.Range(0, RowCells.Count);
            while (LastRowTargetIndex == RowTargetIndex) RowTargetIndex = UnityEngine.Random.Range(0, RowCells.Count);
            LastRowTargetIndex = RowTargetIndex;
            var TargetCell = RowCells[LastRowTargetIndex];
            SetCellSPR(TargetCell, SPR_Target);
            var OtherCells = RowCells.Where(OC => OC.GetInstanceID() != TargetCell.GetInstanceID());
            foreach (GameObject OtherCell in OtherCells) {
                var SPR = GetRandSPR();
                while (SPR == SPR_Target) SPR = GetRandSPR();
                SetCellSPR(OtherCell, SPR);
            }
        }
        if (CM.GameOptions.Direction == ConfigManager.Options.GameDirection.Down)
            LastCorrectKeyVPos = -(ScreenSize.y / 2) - (CellSize.y/2);
        else
            LastCorrectKeyVPos =  (ScreenSize.y / 2) + (CellSize.y/2);

        UnityEngine.Debug.Log("Initial LastCorrectKeyVPos: " + LastCorrectKeyVPos);

        if(OnFinish!=null)
            OnFinish();
    }

    IEnumerator IntroSecuence(Action OnFinish = null) {
        yield return new WaitForSeconds(2);
        foreach (var P in KeyPositions) {
            AM.PlaySFX(SFX_IntroCreationCell);
            var IntroCell = GameObject.Instantiate(Cell);
            IntroCell.name = "IntroCell";
            IntroCell.transform.parent = Pivot.transform;
            IntroCell.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
            IntroCell.GetComponent<RectTransform>().sizeDelta = CellSize;
            IntroCell.GetComponent<RectTransform>().localPosition = new Vector2(0, ScreenSize.y/2+50);
            TweenMovement.Create(IntroCell, P, (T) => {});
            yield return new WaitForSeconds(0.1F);
        }
        yield return new WaitForSeconds(0.5F);
        if (OnFinish != null)
            OnFinish();
    
    }


    void StartSecuence() {
        SPR_Target = GetRandSPR();
        Image TFI = TEX_Fade.GetComponent<Image>();
        iTween.ValueTo(TEX_Fade,1.0F,0.0F,1.0F,(V)=>{
            TFI.color = new Color(1, 1, 1, (float)V);
        },()=>{
            PrepareGrid(() => {
                AM.BGM.clip = BGM_GamePlay;
                AM.BGM.volume = 0.0F;
                AM.BGM_FadeIn();
                StartCoroutine(IntroSecuence(() => {
                    SetCellSPR(PNL_TargetImage, SPR_Target);
                    PNL_TargetImage.gameObject.SetActive(true);
                    PNL_TargetImage.GetComponent<Image>().enabled = true;
                    PNL_TargetImage.GetComponent<Animation>().Play();
                }));
            });
        });
    }

    void TryAgainPanel() {
        var CG = PNL_TryAgain.GetComponent<CanvasGroup>();
        CG.alpha = 0;
        PNL_TryAgain.SetActive(true);
        iTween.ValueTo(PNL_TryAgain, 0.0F, 1.0F, 0.5F, (V) => {
            CG.alpha = (float)V;
            Color CT;
            CT = LBL_Score.GetComponent<Text>().color; CT.a = 1.0F - (float)V; LBL_Score.GetComponent<Text>().color = CT;
            CT = LBL_Timer.GetComponent<Text>().color; CT.a = 1.0F - (float)V; LBL_Timer.GetComponent<Text>().color = CT;
            CT = LBL_Speed.GetComponent<Text>().color; CT.a = 1.0F - (float)V; LBL_Speed.GetComponent<Text>().color = CT;
        }, () => {
            switch (CM.GameOptions.GameMode) {
                case ConfigManager.Options.GameModes.Relay:
                    CurrentScore = CM.GameOptions.InitialScore - CurrentScore;
                    break;
                default: break;
            }

            switch (CM.GameOptions.GameMode) {
                case ConfigManager.Options.GameModes.Rush:
                    LBL_TotalScore.GetComponent<Text>().text = "Time reached: " + string.Format("{0:N2}", (1.00F + SpeedAcum)) + "/s";
                    InputEnabled = true;
                    break;
                default:
                    if (CurrentScore > 0)
                        iTween.ValueTo(LBL_TotalScore, 0, CurrentScore, 1.0F, (V) => {
                            LBL_TotalScore.GetComponent<Text>().text = Math.Truncate(((float)V)).ToString();
                        }, () => {
                            InputEnabled = true;
                        });
                    else
                        InputEnabled = true;
                    break;
            }
        });
    }

    void ContinueBonus() {
        PNL_Continue.SetActive(true);
    }

    void Finish(bool GameFail = false) {
        UnityEngine.Debug.Log("Finish was called with GameFail " + GameFail + " and InputEnabled " + InputEnabled);
        if (BonusContinueShowing) return;
        //if (!InputEnabled) return;
        InputEnabled = false;
       //UnityEngine.Debug.Log("Inside Finish - GameFail: " + GameFail);
        if (InternetAvailable && !BonusContinueUsed && GameFail ) {//&& Advertisement.IsReady()) {
            UnityEngine.Debug.Log("Continue bonus available");
            BonusContinueUsed = true;
            BonusContinueShowing = true;
            stopwatch.Stop();
            ContinueBonus();
        }
        else {
            UnityEngine.Debug.Log("No bonus continue");
            //if (GameFail) throw new Exception("OK");
            if (EnviromentContent != null) EnviromentContent.SetActive(true);
            AM.PlaySFX(GameFail ? SFX_Fail : SFX_Finish);
            GameOver = true;
            var Cells = Grid.GetComponentsInChildren<Transform>().Where(C => C.name.Contains("Cell"));
            foreach (var Cell in Cells) {
                Cell.GetComponent<Rigidbody2D>().gravityScale = GameFail ? 2.0F : -3.0F;
                Cell.GetComponent<Rigidbody2D>().AddForce(new Vector2(UnityEngine.Random.Range(-200.0F, 200.0F), 300.0F));
                GM.DelayAction(5.0f, () => { GameObject.Destroy(Cell.gameObject); });
            }
            AM.BGM_FadeOut(2.0F, () => {
                AM.BGM.clip = BGM_GameResume;
                AM.BGM_FadeIn(1.0F, () => {
                    TryAgainPanel();
                });
            });
        }
    }

    bool FinishedByClick() {
        //UnityEngine.Debug.Log("GameOptions.GameMode: " + CM.GameOptions.GameMode.ToString());
        //UnityEngine.Debug.Log("CurrentScore: " + CurrentScore);
        //UnityEngine.Debug.Log("GameOptions.ScoreGoal: " + CM.GameOptions.ScoreGoal);
        switch (CM.GameOptions.GameMode) { 
            case ConfigManager.Options.GameModes.Classic:
                //UnityEngine.Debug.Log("FinishedByClick true in Classic");
                if (CurrentScore == CM.GameOptions.ScoreGoal) return true;
                break;
            case ConfigManager.Options.GameModes.Relay:
                //UnityEngine.Debug.Log("FinishedByClick true in Relay");
                if (CurrentScore == CM.GameOptions.ScoreGoal) return true;
                break;
        }
        //UnityEngine.Debug.Log("FinishedByClick: " + false);
        return false;
    }

    bool FinishedByTime(TimeSpan t) {
        switch (CM.GameOptions.GameMode) {
            case ConfigManager.Options.GameModes.Zen:
                if (t.TotalSeconds<=0) 
                    Finish(); 
                break;
            case ConfigManager.Options.GameModes.Relay:
                if (t.TotalSeconds <= 0) 
                    Finish();
                break;
        }
        return false;
    }

    //*************** Suppport functions*************

    public Sprite GetRandSPR() {
        int DefaultRndIndex = UnityEngine.Random.Range(0, DefaultImages.Length - 1);
        if (CustomImages.Length > 0) {
            int CustomRndIndex = UnityEngine.Random.Range(0, CustomImages.Length - 1);
            return (UnityEngine.Random.Range(0.0F, 1.0F) > 0.5F) ? DefaultImages[DefaultRndIndex] : CustomImages[CustomRndIndex];
        }
        else {
            return DefaultImages[DefaultRndIndex];
        }
    }

    public void SetCellSPR(GameObject Cell,Sprite SPR){
       Image TEX_Image = Cell.GetComponentsInChildren<Image>(true).Where(IMG => IMG.name == "TEX_Image").FirstOrDefault();
       TEX_Image.enabled = true;
       TEX_Image.overrideSprite = SPR;
    }

    public void FadeText(GameObject LBL,float time=0.3F,bool In = true,Action OnFinish=null) {
        iTween.ValueTo(LBL, In ? 0.0F : 1.0F, In ? 1.0F : 0.0F, time, (V) => {
            var C = LBL.GetComponent<Text>().color;
            C.a = (float)V;
            LBL.GetComponent<Text>().color = C;
        }, OnFinish);
    }
 

    //******************* Threads *******************
    /*
    IEnumerator CheckButtonsClicked() {
        while (true) {
            if (BTNs_Clicked.Count() > 0) {
                var BTN = BTNs_Clicked[BTNs_Clicked.Count - 1];
                BTNs_Clicked.Remove(BTN);
                AddRow();
                yield return new WaitForSeconds(0.31F);
            }
            yield return new WaitForSeconds(0.01F);
        }
    }*/

    private IEnumerator waitThenCallback(float time, Action callback) {
        yield return new WaitForSeconds(time);
        callback();
    }

    void CheckPivotOffset3() {
        if (GameOver || !InputEnabled) return;
        if (!CM.GameOptions.AutoMoove) {
            //if (LastCorrectKeyPos != null && FirstTouched)
                //PivotOffset += 6.0F * (CM.GameOptions.Direction == ConfigManager.Options.GameDirection.Down ? -1 : 1) * CM.GameOptions.Speed;
            if (CM.GameOptions.Direction == ConfigManager.Options.GameDirection.Down && MasterPivot.transform.localPosition.y > PivotOffset) {
                MasterPivot.transform.Translate(0, -1 * Time.deltaTime * CurrentSpeed, 0, Space.Self);
            }
            else if (CM.GameOptions.Direction == ConfigManager.Options.GameDirection.Up && MasterPivot.transform.localPosition.y < PivotOffset) {
                MasterPivot.transform.Translate(0, -1 * Time.deltaTime * CurrentSpeed, 0, Space.Self);
            }
        }
        else if (LastCorrectKeyPos != null && FirstTouched) {
            MasterPivot.transform.Translate(0, (CM.GameOptions.Direction == ConfigManager.Options.GameDirection.Down ? -1 : 1) * Time.deltaTime * CurrentSpeed, 0, Space.Self);
        }
    }


    //Esto detecta si se te paso una key o si no presionaste nada y bajo mucho
    IEnumerator CheckFaiForNoTouchAndMove() {
        while (true) {
            if (GameOver) break;
            if (!InputEnabled) yield return new WaitForFixedUpdate();
            if (LastCorrectKeyPos != null && CM.GameOptions.Direction == ConfigManager.Options.GameDirection.Down && LastCorrectKeyPos.position.y < -9) Finish(true);
            if (LastCorrectKeyPos != null && CM.GameOptions.Direction == ConfigManager.Options.GameDirection.Up && LastCorrectKeyPos.position.y > 9) Finish(true);
            yield return new WaitForFixedUpdate();
        }
    }

    IEnumerator SpeedIncreaser() {
        
        while (true) {
            if (GameOver) break;
            if (!InputEnabled) yield return new WaitForSeconds(1);
            if (FirstTouched) {
                CurrentSpeed += 0.1f;
                SpeedAcum += 0.1F;
                LBL_Speed.GetComponent<Text>().text = string.Format("{0:N2}", (1.00F + SpeedAcum)) + "/s";
            }
            yield return new WaitForSeconds(1);
        }
    
    }

    IEnumerator CheckADS(Action OnFinish) {
        yield return new WaitForSeconds(3);
        //while (Advertisement.isShowing && Advertisement.IsReady()) {
            yield return new WaitForSeconds(1);
        //}
        if (OnFinish != null)
            OnFinish();
    }

    IEnumerator CheckInternetConnection(Action<bool> action) {
        WWW www = new WWW("http://google.com");
        yield return www;
        if (www.error != null) {
            action(false);
        }
        else {
            action(true);
        }
    }

    IEnumerator BonusCountDown(Action OnFinish=null) {
        var T = LBL_CountDownBonus.GetComponent<Text>();
        int CountDown = 3;
        PNL_CountDownBonus.SetActive(true);
        while (CountDown > 0) {
            switch (CountDown) {
                case 3: T.color = Color.red; break;
                case 2: T.color = Color.yellow; break;
                case 1: T.color = Color.green; break;
            }
            AM.PlaySFX(SFX_Fail);
            T.text = CountDown.ToString();
            yield return new WaitForSeconds(1);
            CountDown -= 1;
        }
        PNL_CountDownBonus.SetActive(false);
        if (OnFinish != null) OnFinish();
    }
    //******************* Events*********************
    public decimal FindDifference(decimal nr1, decimal nr2) {
        return Math.Abs(nr1 - nr2);
    }

    void GoodAnswer(GameObject BTN) {
        if (!FinishedByClick()) {
            //UnityEngine.Debug.Log("FinishedByClick was false");
            LastCorrectKeyPos = BTN.GetComponent<RectTransform>();
            if (!CM.GameOptions.AutoMoove)
                AddRow(BTN.GetComponent<RectTransform>());
            else if (!FirstTouched) {
                FirstTouched = true;
                AddRow(BTN.GetComponent<RectTransform>());
            }
            else
                AddRow(BTN.GetComponent<RectTransform>());
            CurrentGoodRowClick += 1;
            if (!CM.GameOptions.AutoMoove)
                PivotOffset += CellSize.y * (CM.GameOptions.Direction == ConfigManager.Options.GameDirection.Down ? -1 : 1);
        }
        else {
            UnityEngine.Debug.Log("FinishedByClick was true");
            Finish();
        }

    }
    
    public void ButtonClick(GameObject BTN) {
        //UnityEngine.Debug.Log("BTN_Clicked: " + BTN.GetInstanceID());
        if (!BTN.GetComponent<Button>().IsInteractable()) return; 
        if (!InputEnabled) return;
        var Grid = GameObject.Find("Grid");
        var Cells = Grid.GetComponentsInChildren<Transform>().Where(C => C.name.Contains("Cell"));
        var CurrentRowCells = Cells.Where(C=>C.GetComponent<RectTransform>().position.y == BTN.GetComponent<RectTransform>().position.y);
        var A = Math.Truncate(BTN.GetComponent<RectTransform>().localPosition.y);
        var B = Math.Truncate(LastCorrectKeyVPos);
        

        double Difference = Convert.ToDouble(FindDifference(Convert.ToDecimal(A),Convert.ToDecimal(B)));
        double Difference2 = Convert.ToDouble(FindDifference(Convert.ToDecimal(Difference), Convert.ToDecimal(CellSize.y)));
        /*UnityEngine.Debug.Log("BTN_Name: " + BTN.name);
        UnityEngine.Debug.Log("LastCorrectKeyVPos: " + LastCorrectKeyVPos);
        UnityEngine.Debug.Log("PivotOffset: " + PivotOffset);
        UnityEngine.Debug.Log("CellSize.y: " + CellSize.y);
        UnityEngine.Debug.Log("Logic: A: " + A + " <> B: " + B);
        UnityEngine.Debug.Log("CellSize.y: " + CellSize.y);
        UnityEngine.Debug.Log("Math.Round(CellSize.y): " + Math.Round(Math.Truncate(CellSize.y)));
        UnityEngine.Debug.Log("Difference1: Between A and B: " + Difference);
        UnityEngine.Debug.Log("Difference2: Between Difference and CellSize.y/4: " + Difference);*/
        var Rejected = !(A != B && (Difference == 0 || Difference2 < CellSize.y/4));// Difference == Math.Round(CellSize.y)));
        if (Rejected) {
            //UnityEngine.Debug.Log("Row rejected");
            //UnityEngine.Debug.Break();
        }
        else { 
            //UnityEngine.Debug.Log("Good row!");
            //UnityEngine.Debug.Break();
            foreach (Transform Cell in CurrentRowCells) {
                Cell.GetComponent<Button>().interactable = false;
            }
            bool WasGoodAnswer = BTN.GetComponentsInChildren<Transform>(true).Where(IMG => IMG.name == "TEX_Image").FirstOrDefault().GetComponent<Image>().overrideSprite.name == SPR_Target.name;
            if (CM.GameOptions.ShowScore) {
                CurrentScore += CM.GameOptions.ScoreForward ? 1 : -1;
            }
            if (WasGoodAnswer) {
                BTN.GetComponentsInChildren<ParticleSystem>().FirstOrDefault().Play();
            }
            AM.PlaySFX(WasGoodAnswer ? SFX_GoodAnswer : SFX_BadAnswer);
            var Colors = BTN.GetComponent<Button>().colors;
            Colors.disabledColor = WasGoodAnswer?Color.green:Color.red;
            Colors.normalColor = WasGoodAnswer ? Color.green : Color.red;
            BTN.GetComponent<Button>().colors = Colors;
            LastCellPressed = BTN;
            if (WasGoodAnswer) 
                GoodAnswer(BTN);
            else
                Finish(true);
        }
    }

    public void OnTargetPanelFinish() {
        UnityEngine.Debug.Log("Finish");
        if (EnviromentContent != null) EnviromentContent.SetActive(false);
        var AllCells = Pivot.GetComponentsInChildren<Transform>(true).Where(C => C.name.Contains("Cell"));
        var IntroCells = AllCells.Where(C=>C.name.Contains("Intro"));
        var GameCells = AllCells.Where(C=>!C.name.Contains("Intro"));
        UnityEngine.Debug.Log("Se encontraron " + GameCells.Count() + " game cells");
        foreach (Transform GC in GameCells)
            GC.gameObject.SetActive(true);
        float Delay = 0.0F;
        foreach (Transform IC in IntroCells) {
            var P = IC.GetComponent<RectTransform>().localPosition;
            var G = IC.gameObject;
            StartCoroutine(waitThenCallback(Delay, () => {
                TweenMovement.Create(G, new Vector2((ScreenSize.x + 100) * (P.x >= 0 ? 1 : -1), P.y), (C) => { 
                    //UnityEngine.Debug.Log("Borrando a " + C.name); 
                    GameObject.Destroy(C); 
                });
            }));
            Delay += 0.1f;
        }
        if (CM.GameOptions.ShowScore) {
            //CurrentScore = GameOptions.ScoreForward?0:
            FadeText(LBL_Score);
        }
        if (CM.GameOptions.ShowTimer)
            FadeText(LBL_Timer);
        if (CM.GameOptions.ShowSpeed)
            FadeText(LBL_Speed);
        LBL_Start.SetActive(true);
        LBL_Start.GetComponent<Animation>().Play();
    }

    public void OnStartLabelFinish() {
        InputEnabled = true;
        LBL_Score.SetActive(CM.GameOptions.ShowScore);
        LBL_Timer.SetActive(CM.GameOptions.ShowTimer);
        LBL_Speed.SetActive(CM.GameOptions.ShowSpeed);
        if (CM.GameOptions.ShowTimer) {
            stopwatch.Start(); 
        }
    }

    public void BTNS_TryAgain(GameObject BTN) {
        if (!InputEnabled) return;
        AM.PlaySFX(BTN.name.Contains("Try") ? SFX_BTN_TryAgain : SFX_BTN_GoBack);
        var CG = PNL_TryAgain.GetComponent<CanvasGroup>();
        iTween.ValueTo(PNL_TryAgain, CG.alpha, 0.0F,0.25F, (V) => {
            CG.alpha = (float)V;
        }, () => {
            GM.DelayAction(1.0F, () => {
                        Image TFI = TEX_Fade.GetComponent<Image>();
                        iTween.ValueTo(TEX_Fade, 0.0F, 1.0F, 1.0F, (V) => {
                            TFI.color = new Color(1, 1, 1, (float)V);
                        }, () => {
                            AM.BGM_FadeOut(1.0F, () => {
                                if (AdMobInterstitial.IsLoaded()){
                                    AdMobInterstitial.OnAdClosed += (a, b) => { SceneManager.LoadScene(BTN.name.Contains("Try") ? SceneManager.GetActiveScene().name : "LoginPage"); };
                                    AdMobInterstitial.Show();
                                } 
                                else
                                    SceneManager.LoadScene(BTN.name.Contains("Try") ? SceneManager.GetActiveScene().name : "LoginPage");

                                /*if (Advertisement.IsReady()) {
                                    Advertisement.Show();
                                    StartCoroutine(CheckADS(() => {
                                        SceneManager.LoadScene(BTN.name.Contains("Try") ? SceneManager.GetActiveScene().name : "LoginPage");
                                    }));
                                }
                                else
                                    SceneManager.LoadScene(BTN.name.Contains("Try") ? SceneManager.GetActiveScene().name : "LoginPage");*/
 
                            });
                        });
            });
        });
    }

    public void BTNS_ContinueBonus(GameObject BTN) {
        UnityEngine.Debug.Log(BTN.name + " clicked");
        PNL_Continue.SetActive(false);
        
        if (!BTN.name.Contains("NoContinue")) {
            /*if (Advertisement.IsReady()) {
                StartCoroutine(CheckADS(() => {
                    StartCoroutine(BonusCountDown(() => {
                        GoodAnswer(LastCellPressed);
                        stopwatch.Start();
                        InputEnabled = true;
                        BonusContinueShowing = false;
                    }));
                }));
                Advertisement.Show();*/
            //}
            //else {
                BonusContinueShowing = false; Finish(true);
            //}
        }
        else {
            BonusContinueShowing = false; Finish(true);
        }
    }
    //***************** Others **********************
    public void StopwatchController() {
        if (stopwatch == null) return;
        if (stopwatch.IsRunning && !GameOver) {
            var t = new System.TimeSpan(CM.GameOptions.TimerStart.Hours, CM.GameOptions.TimerStart.Minutes, CM.GameOptions.TimerStart.Seconds);
            t = CM.GameOptions.TimerForward ? t.Add(stopwatch.Elapsed) : t.Subtract(stopwatch.Elapsed);
            LBL_Timer.GetComponent<Text>().text = "Time: " + t.Minutes + ":" + t.Seconds + ":" + t.Milliseconds;
            FinishedByTime(t);
        } 
    }


    //**************** ADS Related *****************

    private void PrepareAdMobInt() {
        #if UNITY_ANDROID
                string adUnitId = "ca-app-pub-5164871131952640/3627809368";
        #elif UNITY_IPHONE
                string adUnitId = "ca-app-pub-5164871131952640/3627809368";
        #else
                string adUnitId = "unexpected_platform";
        #endif

        // Initialize an InterstitialAd.
        AdMobInterstitial = new InterstitialAd(adUnitId);

        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the interstitial with the request.
        AdMobInterstitial.LoadAd(request);
    }

    //***********************************************

	// Use this for initialization

    void Awake() {
        TEX_Fade.GetComponent<Image>().color = new Color(1,1,1,1);
        PrepareComponents();
    }
    
    void Start () {
        StartCoroutine(CheckInternetConnection((isConnected) => {InternetAvailable = isConnected;}));
        string UnityADS_AppID = "1019258";
        switch(Application.platform){
            case RuntimePlatform.Android: UnityADS_AppID = "1019258"; break;
            case RuntimePlatform.IPhonePlayer: UnityADS_AppID = "1019266"; break;
        }
        PrepareAdMobInt();
        //Advertisement.Initialize(UnityADS_AppID, Application.isEditor);
        Courtain = GameObject.Find("Courtain");
        Cell = GameObject.Find("Cell");
        Grid = GameObject.Find("Grid");
        Pivot = GameObject.Find("Pivot");
        MasterPivot = GameObject.Find("MasterPivot");
        LBL_Score = GameObject.Find("LBL_Score");
        LBL_Timer = GameObject.Find("LBL_Timer");
        LBL_Speed = GameObject.Find("LBL_Speed");

        PNL_TargetImage = Grid.GetComponentsInChildren<Transform>(true).Where(G => G.name == "PNL_TargetImage").FirstOrDefault().gameObject;
        LBL_Start = Grid.GetComponentsInChildren<Transform>(true).Where(G => G.name == "LBL_Start").FirstOrDefault().gameObject;
        ConfigManager CM = GameObject.Find("ConfigManager").GetComponent<ConfigManager>();
        if (CM.GameOptions == null) {
            //Clasic Test
            CM.GameOptions = CM.GameOptions??new ConfigManager.Options(ConfigManager.Options.GameModes.Classic,"8",ConfigManager.Options.GameDifficulty.Normal);
            //Arcade Test
            //CM.GameOptions = CM.GameOptions??new ConfigManager.Options(ConfigManager.Options.GameModes.Arcade, "Normal", ConfigManager.Options.GameDifficulty.Normal);
            //Zen Test
            //CM.GameOptions = CM.GameOptions ?? new ConfigManager.Options(ConfigManager.Options.GameModes.Zen, "30", ConfigManager.Options.GameDifficulty.Normal);
            //Rush Test
            //CM.GameOptions = CM.GameOptions ?? new ConfigManager.Options(ConfigManager.Options.GameModes.Rush, "Normal", ConfigManager.Options.GameDifficulty.Normal);
            //Relay Test
            //CM.GameOptions = CM.GameOptions ?? new ConfigManager.Options(ConfigManager.Options.GameModes.Relay, "8", ConfigManager.Options.GameDifficulty.Normal);
        }
        UnityEngine.Debug.Log("CM " + CM);
        UnityEngine.Debug.Log("GameOptions " + CM.GameOptions);
        CurrentScore = CM.GameOptions.InitialScore;
        CurrentSpeed = CM.GameOptions.Speed;


        UnityEngine.Debug.Log("Starting GameMode: " + CM.GameOptions.GameMode.ToString());
        UnityEngine.Debug.Log("Starting Difficulty: " + CM.GameOptions.Difficulty.ToString());
        UnityEngine.Debug.Log("Starting Direction: " + CM.GameOptions.Direction.ToString());
        UnityEngine.Debug.Log("GameOptions.ShowTimer: " + CM.GameOptions.ShowTimer);
        UnityEngine.Debug.Log("GameOptions.ShowScore: " + CM.GameOptions.ShowScore);
        UnityEngine.Debug.Log("GameOptions.ShowSpeed: " + CM.GameOptions.ShowSpeed);
        UnityEngine.Debug.Log("GameOptions.TimerStart: " + CM.GameOptions.TimerStart.ToString());
        UnityEngine.Debug.Log("GameOptions.ScoreGoal: " + CM.GameOptions.ScoreGoal.ToString());
        UnityEngine.Debug.Log("EIM.UserTextures count: " + EIM.UserTextures.Count());

        foreach (Texture2D T in EIM.UserTextures) {
            var SPR = Sprite.Create(T, new Rect(0, 0, T.width, T.height), new Vector2(0.5f, 0.5f));
            Array.Resize(ref CustomImages, CustomImages.Length+1);
            CustomImages[CustomImages.Length - 1] = SPR;
        }
       

        //StartCoroutine(CheckPivotOffset());
        
        //AM.PlayBGM(BGM_Main);
        if (CM.GameOptions.AutoMoove)
            StartCoroutine(CheckFaiForNoTouchAndMove());
        if (CM.GameOptions.IncreaseSpeed)
            StartCoroutine(SpeedIncreaser());
        StartSecuence();
	}
	
	// Update is called once per frame
	void Update () {
        TweenMovement.UpdateAll();

        //UnityEngine.Debug.Log(stopwatch);
        //UnityEngine.Debug.Log(GameOver);
        StopwatchController();

        if (CM.GameOptions.ShowScore)
            LBL_Score.GetComponent<Text>().text = (CM.GameOptions.ScoreForward ? "Score: " : "Remain: ") + CurrentScore;

        if (Input.GetKeyDown(KeyCode.T) && InputEnabled)
            Finish(true);

        if (LastCorrectKeyPos != null) LastCorrectKeyPosY = LastCorrectKeyPos.position.y;
        CheckPivotOffset3();

            

	}
}
