﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;
using System.Collections.Generic;
public class GlobalManager : MonoBehaviour {


    public Transform SearchTransform(Transform root, string Name, bool CaseSensitve = true) {
        Component[] CMPS = root.GetComponentsInChildren(typeof(Transform), true);
        Component x = CMPS.Where(C => (CaseSensitve ? C.name == Name : C.name.Contains(Name))).FirstOrDefault();
        return x != null ? x.transform : null;
    }

    public Transform[] SearchTransforms(Transform root, string Name, bool CaseSensitve = true,bool IncludeInactive = true) {
        var R = new List<Transform>();
        Component[] CMPS = root.GetComponentsInChildren(typeof(Transform), IncludeInactive);
        CMPS = CMPS.Where(C => (CaseSensitve ? C.name == Name : C.name.Contains(Name))).ToArray();
        foreach (Component C in CMPS)
            R.Add(C.transform);
        return R.ToArray();
    }

    IEnumerator ActionDelayed(float seconds, Action Action) {
        yield return new WaitForSeconds(seconds);
        Action();
    }
    IEnumerator ActionFrameDelayed(int frames, Action Action) {
        int N = 0;
        while (N < frames) {
            yield return new WaitForEndOfFrame(); N++;
        }
        Action();
    }

    public void DelayAction(float seconds, Action Action) {
        StartCoroutine(ActionDelayed(seconds, Action));
    }
    public void DelayFrameAction(int frames, Action Action) {
        StartCoroutine(ActionFrameDelayed(frames, Action));
    }


    public static void CheckGlobals() {
        if (GameObject.Find("GlobalObjects") == null) {
            GameObject GO = (GameObject)Instantiate(Resources.Load("GlobalObjects", typeof(GameObject)));
            GO.name = GO.name.Replace("(Clone)", "");
        }
    }

    void Awake(){
    
    
    }

	// Use this for initialization
	void Start () {
        //Debug.Log(transform.parent.gameObject.name);
        Application.runInBackground = true;
        DontDestroyOnLoad(transform.parent.gameObject);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
