﻿using UnityEngine;
using System.Collections;
using System;

public class FileBrowserManager : MonoBehaviour {

    //skins and textures
    public GUISkin[] skins;
    public Texture2D file, folder, back, drive;
    public bool Display = false;

    string[] layoutTypes = { "Type 0", "Type 1" };
    
    //initialize file browser
    FileBrowser fb;
    string output = "no file";

    //Events
    public Action<string> OnSelect;
    public Action OnCancel;


    public void Show(Action<string> OnSelect,Action OnCancel=null){
        this.OnSelect = OnSelect;
        this.OnCancel = OnCancel;
        Display = true;
    }


    void Awake() {
        fb = new FileBrowser();
    }



    void Start() {
        //setup file browser style
        fb.guiSkin = skins[0]; //set the starting skin
        //set the various textures
        fb.fileTexture = file;
        fb.directoryTexture = folder;
        fb.backTexture = back;
        fb.driveTexture = drive;
        //show the search bar
        fb.showSearch = false;
        //search recursively (setting recursive search may cause a long delay)
        fb.searchRecursively = true;
    }

    void OnGUI() {
        if (Display) {
            if (fb.draw()) {
                Display = false;
                if (fb.outputFile == null) {
                    Debug.Log("No file selected");
                    if (OnCancel != null) OnCancel();
                }
                else {
                    Debug.Log("File selected: " + fb.outputFile.ToString());
                    Debug.Log("File FullName selected: " + fb.outputFile.FullName);
                    OnSelect(fb.outputFile.FullName);
                }
            }
        }
    }
}
