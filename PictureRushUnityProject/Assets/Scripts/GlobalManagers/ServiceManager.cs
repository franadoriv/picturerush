﻿using UnityEngine;
using System.Collections;
using System.Xml.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using SimpleJson;
using FullSerializer;
using Newtonsoft.Json;
public class ServiceManager : MonoBehaviour {

    //string ServiceUrl = "http://localhost/ShoppingGameServer/SPSVC.ASMX/";
    
    string ServiceUrl = "http://styloh-001-site1.smarterasp.net/PhotoRush/SPSVC.asmx/";
    string GServicesUrl = "http://styloh-001-site1.smarterasp.net/globalservices/index.php/globalfunctions/";
    
    public Texture2D OnErrorImage;

    public List<int> SelectedPictures = new List<int>();
    //****************** Object Class ***********************

    public class PictureStatus{
        public int PictureID {get; set;}
        public bool Active {get; set;}

        public PictureStatus(int PictureID, bool Active) { this.PictureID = PictureID; this.Active = Active; }
    }

    public class PlayerScore {
        public string PlayerName { get; set; }
        public int TotalScore { get; set; }

        public List<string> PlayerPhotos { get; set; }
    }

    public class DeviceInfo {

        public string application = Application.productName;
        public string action { get; set; }
        public string device_id = SystemInfo.deviceUniqueIdentifier;
        public string device_type = SystemInfo.deviceType.ToString();
        public string device_model = SystemInfo.deviceModel;
        public string device_os = SystemInfo.operatingSystem;

        public DeviceInfo(string Action) {
            this.action = Action;
        }
    }

    //******************* Request ***************************

    public class DeviceInfoRequest {
        public DeviceInfo DeviceInfo = new DeviceInfo("LoginRequest");
    }

    //******************* Responses ***************************
    public class WSResponse {
        public string ErrMsg { get; set; }
        public string Result { get; set; }
    }

    //*************** Support Functions *********************
    public static class StringSerializationAPI {
        private static readonly fsSerializer _serializer = new fsSerializer();

        public static string Serialize(Type type, object value) {
            // serialize the data
            fsData data;
            _serializer.TrySerialize(type, value, out data).AssertSuccessWithoutWarnings();

            // emit the data via JSON
            return fsJsonPrinter.CompressedJson(data);
        }

        public static object Deserialize(Type type, string serializedState) {
            // step 1: parse the JSON data
            fsData data = fsJsonParser.Parse(serializedState);

            // step 2: deserialize the data
            object deserialized = null;
            _serializer.TryDeserialize(data, type, ref deserialized).AssertSuccessWithoutWarnings();

            return deserialized;
        }
    }

    //************ Generic WenService call *******************
    public IEnumerator CallWS<T>(string Route, string Method, object JData, Action<T> OnFinish = null, Action<string> OnError = null) {
        Debug.Log("Calling to: " + GServicesUrl + Route);
        WWW request;
        if (JData != null) {
            WWWForm data = new WWWForm();
            data.AddField("JSON", JsonConvert.SerializeObject(JData));
            request = new WWW(GServicesUrl + Route, data);
        }
        else {
            request = new WWW(GServicesUrl + Route);
        }

        yield return request;
        if (!string.IsNullOrEmpty(request.error)) {
            print(request.error);
            Debug.Log("Error: " + request.error);
            if (OnError != null)
                OnError("WebService call error: " + request.error);
        }
        else {
            Debug.Log("From Server: " + request.text);
            WSResponse Response = null;
            try {
                Response = JsonConvert.DeserializeObject<WSResponse>(request.text);
            }
            catch (Exception e) {
                if (OnError != null)
                    OnError("Response parse error: " + e.Message);
            }
            if (Response != null) {
                if (!String.IsNullOrEmpty(Response.ErrMsg)) {
                    if (OnError != null)
                        OnError(Response.ErrMsg);
                }
                else if (String.IsNullOrEmpty(Response.ErrMsg)) {
                    if (OnFinish != null)
                        OnFinish(JsonConvert.DeserializeObject<T>(request.text));
                }
            }

        }
    }

    
    //*************** Login related Calls *******************

    public IEnumerator DoLogin(string EMail, string Password, string FBID, string TWID,string GOID,string Name,string LastName,Action<int> OnFinish,Action<String> OnError=null) {
        var form = new WWWForm();
        form.AddField("EMail", EMail);
        form.AddField("Password", Password);
        form.AddField("FBID", FBID);
        form.AddField("TWID", TWID);
        form.AddField("GOID", GOID);
        form.AddField("Name", Name);
        form.AddField("LastName", LastName);
        Debug.Log("Calling to: " + ServiceUrl + "DoLogin");
        var request = new WWW(ServiceUrl + "DoLogin", form);
        yield return request;
        if (!string.IsNullOrEmpty(request.error)) {
            Debug.Log("Error calling WS: " + request.error);
            if (OnError != null)
                OnError(request.error);
        }
        else {
            Debug.Log("SR: " + request.text);
            XDocument doc = XDocument.Parse(request.text);
            XNamespace ns = "http://tempuri.org/";
            XElement ErrMess;
            XElement StackTrace;
            if ((ErrMess = doc.Root.Element(ns + "ErrMess")) != null) {
                StackTrace = doc.Root.Element(ns + "StackTrace");
                Debug.Log("Ocurrio un error: " + ErrMess.Value + " StackTrace: " + StackTrace.Value);
                if (OnError != null)
                    OnError(ErrMess.Value);
            }
            else {
                int UserID = Convert.ToInt32(doc.Root.Element(ns+"UserAccount").Element(ns + "UserID").Value);
                Debug.Log("UserID: " + UserID);
                OnFinish(UserID);
            }
        }
    }

    //*************** Picture's related Calls *******************

    public IEnumerator GetPictureGroups(int UserID,Action<List<objPictureGroup>> OnFinish,Action<String> OnError=null) {
        Debug.Log("GetPictureGroups WS Function Called");
        var form = new WWWForm();
        form.AddField("UserID", UserID);
        Debug.Log("Calling to: " + ServiceUrl + "GetPictureGroups");
        var request = new WWW(ServiceUrl + "GetPictureGroups", form);
        yield return request;
        if (!string.IsNullOrEmpty(request.error)) {
            Debug.Log("Error calling WS: " + request.error);
            if (OnError != null)
                OnError(request.error);
        }
        else {
            Debug.Log("SR: " + request.text);
            XDocument doc = XDocument.Parse(request.text);
            XNamespace ns = "http://tempuri.org/";
            XElement ErrMess;
            XElement StackTrace;
            if ((ErrMess = doc.Root.Element(ns + "ErrMess")) != null) {
                StackTrace = doc.Root.Element(ns + "StackTrace");
                Debug.Log("Ocurrio un error: " + ErrMess.Value + " StackTrace: " + StackTrace.Value);
                if (OnError != null)
                    OnError(ErrMess.Value);
            }
            else {
                var Groups = doc.Root.Descendants(ns + "objPictureGroup");
                var PictureGroups = new List<objPictureGroup>();
                foreach (XElement Group in Groups) {
                    var PG = new objPictureGroup(Convert.ToInt32(Group.Element(ns + "PictureGroupID").Value), Group.Element(ns + "PictureGroupName").Value);
                    foreach (XElement Picture in Group.Element(ns+"Pictures").Elements(ns + "objPicture")) {
                        PG.Pictures.Add(
                            new objPicture(
                                Convert.ToInt32(Picture.Element(ns + "PictureID").Value), 
                                Picture.Element(ns + "PictureName").Value,
                                Convert.ToDateTime(Picture.Element(ns + "DateStored").Value),
                                Convert.ToBoolean(Picture.Element(ns + "IncludeOnGame").Value)
                                ));
                    }
                    PictureGroups.Add(PG);
                }
                OnFinish(PictureGroups);
            }
        }
    
    }

    public IEnumerator GetPicture(int PictureID, Action<Texture2D> OnFinish,Action<string> OnError = null) {
        Debug.Log("GetPicture WS Function Called");
        var T = new Texture2D(4, 4, TextureFormat.DXT1, false);
        var request = new WWW(ServiceUrl + "ShowPicture?PictureID=" + PictureID);
        yield return request;
        if (!string.IsNullOrEmpty(request.error)) {
            Debug.Log("Error calling WS: " + request.error);
            T = OnErrorImage;
        }
        else {
            request.LoadImageIntoTexture(T);
        }
        yield return new WaitForEndOfFrame();
        if (OnFinish != null)
            OnFinish(T);
    }

    public IEnumerator GetUserPictures(int UserID, bool IncludeImageData = false, bool OnlySelected = false, Action<List<objPicture>> OnFinish = null, Action<string> OnError = null) {
        Debug.Log("GetUserPictures WS Function Called");
        string URL = ServiceUrl + "GetUserPictures?UserID=" + UserID + "&IncludeImageData=" + IncludeImageData + "&OnlySelected=" + OnlySelected;
        Debug.Log(URL);
        var request = new WWW(URL);
        yield return request;
        if (!string.IsNullOrEmpty(request.error)) {
            Debug.Log("Error calling WS: " + request.error);
            Debug.Log("SR: " + request.text);
            if (OnError != null)
                OnError(request.error);
        }
        else {
            Debug.Log(request.text);
            XDocument doc = XDocument.Parse(request.text);
            XNamespace ns = "http://tempuri.org/";
            XElement ErrMess;
            XElement StackTrace;
            if ((ErrMess = doc.Root.Element(ns + "ErrMess")) != null) {
                StackTrace = doc.Root.Element(ns + "StackTrace");
                Debug.Log("Ocurrio un error: " + ErrMess.Value + " StackTrace: " + StackTrace.Value);
                if (OnError != null)
                    OnError(ErrMess.Value);
            }
            else {
                var ServerPictures = doc.Root.Descendants(ns + "objPicture");
                var Pictures = new List<objPicture>();
                Debug.Log("Pictures gained! " + ServerPictures.Count());
                if (ServerPictures.Count() > 0) {
                    foreach (XElement ServerPicture in ServerPictures) {
                        XElement Picture = ServerPicture;//ServerPicture.Element(ns + "objPicture");
                        if (Picture == null) continue;
                        Debug.Log(Picture.ToString());
                        Debug.Log("PictureID: " + Picture.Element(ns + "PictureID"));
                        Debug.Log("PictureName: " + Picture.Element(ns + "PictureName"));
                        Debug.Log("DateStored: " + Picture.Element(ns + "DateStored"));
                        Debug.Log("IncludeOnGame: " + Picture.Element(ns + "IncludeOnGame"));
                        Pictures.Add(
                                new objPicture(
                                    Convert.ToInt32(Picture.Element(ns + "PictureID").Value),
                                    Picture.Element(ns + "PictureName").Value,
                                    Convert.ToDateTime(Picture.Element(ns + "DateStored").Value),
                                    Convert.ToBoolean(Picture.Element(ns + "IncludeOnGame").Value),
                                    Picture.Element(ns + "Base64Data").Value
                                    ));
                    }  
                }
                OnFinish(Pictures);
            }
            yield return new WaitForEndOfFrame();
        }
    }

    public IEnumerator UploadPicture(int UserID,int GroupID,string ImageName,string Path,Action OnFinish,Action<string> OnError=null) {
        /*WWW localFile = new WWW("file:///" + Path);
        yield return localFile;
        if (localFile.error == null)
            Debug.Log("Loaded file successfully");
        else {
            Debug.Log("Open file error: " + localFile.error);
            if (OnError != null)
                OnError(localFile.error);
            yield break; // stop the coroutine here
        }
        Texture2D Texture = localFile.texture;*/
        Debug.Log("UploadPicture WS Function Called");
        var Texture = FileBrowser.LoadPNG(Path);
        byte[] PictureBytes = Texture.EncodeToPNG();
        string enc = Convert.ToBase64String(PictureBytes);
        //string enc = Convert.ToBase64String(FileBrowser.LoadImageBytes(Path));
        Debug.Log(enc);
        WWWForm postForm = new WWWForm();
        postForm.AddField("UserID", UserID);
        postForm.AddField("GroupID", GroupID);
        postForm.AddField("ImageName", ImageName);
        postForm.AddField("Base64", enc);
        WWW upload = new WWW(ServiceUrl + "SavePicture", postForm);  
        yield return upload;
        if (upload.error == null) {
            Debug.Log("upload done :" + upload.text);
            if (!upload.text.Contains("http://tempuri.org/") && OnError != null)
                OnError("Failed on WS Response: " + upload.text.Substring(0, 100)+"...");
            else { 
                XDocument doc = XDocument.Parse(upload.text);
                XNamespace ns = "http://tempuri.org/";
                XElement ErrMess;
                XElement StackTrace;
                if ((ErrMess = doc.Root.Element(ns + "ErrMess")) != null) {
                    StackTrace = doc.Root.Element(ns + "StackTrace");
                    Debug.Log("Ocurrio un error: " + ErrMess.Value + " StackTrace: " + StackTrace.Value);
                    if (OnError != null)
                        OnError(ErrMess.Value);
                }
                else {
                    if (OnFinish != null)
                        OnFinish();
                }
            }
        }
        else {
            Debug.Log("Error during upload: " + upload.error);
            if (OnError != null)
                OnError(upload.error);
        }
    }

    public IEnumerator DeletePicture(int PictureID, Action OnFinish, Action<string> OnError = null) {
        Debug.Log("DeletePicture WS Function Called");
        WWWForm postForm = new WWWForm();
        postForm.AddField("PictureID", PictureID);
        WWW request = new WWW(ServiceUrl + "DeletePicture", postForm);
        yield return request;
        if (request.error == null) {
            if (!request.text.Contains("http://tempuri.org/") && OnError != null) {
                Debug.Log("Error during deletion: " + request.text);
                OnError("Failed on WS Response: " + request.text.Substring(0, 100) + "...");
            }
            else {
                XDocument doc = XDocument.Parse(request.text);
                XNamespace ns = "http://tempuri.org/";
                XElement ErrMess;
                XElement StackTrace;
                if ((ErrMess = doc.Root.Element(ns + "ErrMess")) != null) {
                    StackTrace = doc.Root.Element(ns + "StackTrace");
                    Debug.Log("Ocurrio un error: " + ErrMess.Value + " StackTrace: " + StackTrace.Value);
                    if (OnError != null)
                        OnError(ErrMess.Value);
                }
                else {
                    if (OnFinish != null)
                        OnFinish();
                }
            }
        }
        else {
            Debug.Log("Error during deletion: " + request.error);
            if (OnError != null)
                OnError(request.error);
        }
    }

    public IEnumerator SetPicturesStatus(List<PictureStatus> Pictures, Action OnFinish, Action<string> OnError = null) {
        Debug.Log("SetPicturesStatus WS Function Called");
        WWWForm postForm = new WWWForm();
        string JsonPicturesStatus = StringSerializationAPI.Serialize(Pictures.GetType(), Pictures);
        Debug.Log("Sending: " + JsonPicturesStatus);
        postForm.AddField("JsonPicturesStatus", JsonPicturesStatus);
        WWW request = new WWW(ServiceUrl + "SetPicturesStatus", postForm);
        yield return request;
        if (request.error == null) {
            if (!request.text.Contains("http://tempuri.org/") && OnError != null) {
                Debug.Log("Error during picture status update: " + request.text);
                OnError("Failed on WS Response: " + request.text.Substring(0, 100) + "...");
            }
            else {
                XDocument doc = XDocument.Parse(request.text);
                XNamespace ns = "http://tempuri.org/";
                XElement ErrMess;
                XElement StackTrace;
                if ((ErrMess = doc.Root.Element(ns + "ErrMess")) != null) {
                    StackTrace = doc.Root.Element(ns + "StackTrace");
                    Debug.Log("Ocurrio un error: " + ErrMess.Value + " StackTrace: " + StackTrace.Value);
                    if (OnError != null)
                        OnError(ErrMess.Value);
                }
                else {
                    if (OnFinish != null)
                        OnFinish();
                }
            }
        }
        else {
            Debug.Log("Error during picture status update: " + request.error);
            if (OnError != null)
                OnError(request.error);
        }
    }

    //*************** Others Calls *******************

    public void DeviceRegister(Action<WSResponse> OnFinish = null, Action<string> OnError = null) {
        var Request = new DeviceInfoRequest();
        StartCoroutine(CallWS<WSResponse>("device_register", "POST", Request, OnFinish, OnError));
    }


}

