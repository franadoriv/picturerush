﻿using UnityEngine;
using System.Collections;
using System;

public class ConfigManager : MonoBehaviour {



    public Options GameOptions;
    public Options.GameModes CurrentGameMode = Options.GameModes.Classic;
    public Options.GameDifficulty CurrentGameDifficulty = Options.GameDifficulty.Normal;
    public int CurrentScoreGoal = 8;
    public int CurrentSeconds = 30;
   
    
    public bool MuteSound = false;
    public float TilesSpeed = 30;
    public int MaxCorrectTiles = 50;
    public int TimeOutSeconds = 30;
    public bool IsConnected = false;
    public bool PassedThroughLogin = false;

    public class Options {
        public enum GameDifficulty { Easy, Normal, Hard, Superman } //Reeady
        public enum GameModes { Classic, Arcade, Zen, Rush, Relay };
        public enum GameDirection { Down, Up } //Reeady

        
        //Game properties
        public GameModes GameMode;
        public GameDifficulty Difficulty;

        public int ColCant = 4; //Ready
        public int RowCant = 4; //Ready
        public int ScoreGoal = 0;
        //public int TimeGoal = 0; //Seconds


        public float Speed = 6.0F;
        public bool AutoStart = false;
        public GameDirection Direction = GameDirection.Down; //Ready
        public bool AutoMoove = false;

        public int InitialScore = 0;
        public bool ShowScore = false; //Ready
        public bool ScoreForward = true;//Ready

        public bool ShowTimer = false; //Ready
        public bool TimerForward = true; //Ready

        public bool ShowSpeed = false;//Ready
        public bool IncreaseSpeed = false;

        public TimeSpan TimerStart = new TimeSpan(); //Ready

        public Options(GameModes GameMode = GameModes.Classic, string OptionSelected = "", GameDifficulty Difficulty = GameDifficulty.Normal) {
            Debug.Log("GameMode: " + GameMode.ToString() + " - OptionSelected: " + OptionSelected);
            this.GameMode = GameMode;
            this.Difficulty = Difficulty;
            this.Speed = this.Difficulty == GameDifficulty.Easy ? 12 : 4;
            switch (GameMode) {
                case GameModes.Classic:
                    this.AutoStart = false;
                    this.Direction = GameDirection.Down;
                    this.AutoMoove = false;
                    this.ShowScore = true;
                    this.ShowTimer = true;
                    this.TimerForward = true; 
                    int.TryParse(OptionSelected, out this.ScoreGoal);
                    break;
                case GameModes.Arcade:
                    this.AutoStart = false;
                    Debug.Log("Arcade-OptionSelected: " + OptionSelected);
                    this.Direction = (OptionSelected.ContainsAny("Normal", "Faster") || String.IsNullOrEmpty(OptionSelected)) ? GameDirection.Down : GameDirection.Up;
                    this.AutoMoove = true;
                    this.ShowScore = true;
                    this.ShowTimer = false;
                    break;
                case GameModes.Zen:
                    this.AutoStart = false;
                    this.Direction = GameDirection.Down;
                    this.AutoMoove = false;
                    this.ShowScore = true;
                    this.ShowTimer = true;
                    this.TimerForward = false;
                    TimeSpan.TryParse("0:0:" + OptionSelected, out this.TimerStart);
                    break;
                case GameModes.Rush:
                    this.AutoStart = false;
                    this.Direction = (OptionSelected.Contains("Normal") || String.IsNullOrEmpty(OptionSelected)) ? GameDirection.Down : GameDirection.Up;
                    this.AutoMoove = true;
                    this.ShowScore = false;
                    this.ShowTimer = false;
                    this.ShowSpeed = true;
                    this.IncreaseSpeed = true;
                    break;
                case GameModes.Relay:
                    this.AutoStart = false;
                    this.Direction = GameDirection.Down;
                    this.AutoMoove = false;
                    this.ShowScore = true;
                    this.ScoreForward = false;
                    this.ShowTimer = true;
                    this.TimerForward = false;
                    this.ScoreGoal = 0;
                    this.InitialScore = 60;
                    TimeSpan.TryParse("0:0:" + OptionSelected, out this.TimerStart);
                    break;
            }
            switch (Difficulty) {
                case GameDifficulty.Easy: this.ColCant = 2; this.RowCant = 2; break;
                case GameDifficulty.Normal: this.ColCant = 4; this.RowCant = 4; break;
                case GameDifficulty.Hard: this.ColCant = 6; this.RowCant = 6; break;
                case GameDifficulty.Superman: this.ColCant = 8; this.RowCant = 8; break;
            }
        }

    }


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
