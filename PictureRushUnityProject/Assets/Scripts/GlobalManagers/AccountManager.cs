﻿using UnityEngine;
using System.Collections;
//using Soomla.Profile;
using System;
//using Facebook.Unity;
using System.Collections.Generic;

public class AccountManager : MonoBehaviour {

    MainSceneCode MSC;
    public int UserID;


    void PrepareComponents() {
        MSC = GameObject.Find("MainSceneCode").GetComponent<MainSceneCode>();
    }


    void PrepareHandlers() {
        Debug.Log("FB PrepareHandlers");
        //Debug.Log("FB.IsInitialized: " + FB.IsInitialized);
        /*if (!FB.IsInitialized) {
            // Initialize the Facebook SDK
            FB.Init(() => {
                Debug.Log("FB InitDelegate");
                if (FB.IsInitialized) {
                    // Signal an app activation App Event
                    FB.ActivateApp();
                    // Continue with Facebook SDK
                    // ...
                }
                else {
                    Debug.Log("Failed to Initialize the Facebook SDK");
                }
            }, (isGameShown) => {
                Debug.Log("FB isGameShown: " + isGameShown);
                if (!isGameShown) {

                    // Pause the game - we will need to hide
                    Time.timeScale = 0;
                }
                else {
                    // Resume the game - we're getting focus again
                    Time.timeScale = 1;
                }
            });
        }
        else {
            // Already initialized, signal an app activation App Event
            FB.ActivateApp();
        }
        */
     
    }

    public void FBLogin() {
        Action<int> OnServerResponse = (UserID) => {
            MSC.CM.IsConnected = true;
            this.UserID = UserID;
            Action F = () => {
                MSC.ALTM.ClosePleaseWait(() => {
                    MSC.MIM.ChangeToPanelByName("PNL_MainPanel");
                });
            };
            MSC.SVCM.DeviceRegister((R) => { F(); }, (E) => { F(); });
        };
        Action<string> OnServerError = (ErrMess) => {
            MSC.CM.IsConnected = false;
            if (ErrMess.Contains("LOGIN_CANCELLED")) {
                MSC.ALTM.ClosePleaseWait(() => {
                    MSC.MIM.ChangeToPanelByName("PNL_Login");
                });
            }
            else {
                Debug.Log("Login error: " + ErrMess);
                MSC.ALTM.ClosePleaseWait(() => {
                    MSC.ALTM.ShowB("Error", ErrMess, "Ok", () => {
                        MSC.MIM.ChangeToPanelByName("PNL_Login");
                    });
                });
            }
        };
        //SoomlaProfile.Login(Provider.FACEBOOK);
        var perms = new List<string>() { "public_profile", "email", "user_friends" };
        //FB.LogInWithReadPermissions(perms, (result) => 
        //{
        //    var R = (ILoginResult)result;
        //    if (FB.IsLoggedIn) {
        //        // AccessToken class will have session details
        //        var aToken = Facebook.Unity.AccessToken.CurrentAccessToken;
        //        // Print current access token's User ID
        //        Debug.Log(aToken.UserId);
        //        // Print current access token's granted permissions
        //        foreach (string perm in aToken.Permissions) {
        //            Debug.Log(perm);
        //        }
        //        FB.API("/me?fields=name,email", HttpMethod.GET, graphResult => {
        //            foreach (var FBV in graphResult.ResultDictionary) {
        //                Debug.Log(FBV.Key + " -> " + FBV.Value.ToString());
        //            }
        //            if (!string.IsNullOrEmpty(graphResult.Error)) {
        //                Debug.Log("Could not get user data:" + graphResult.Error);
        //                OnServerError("Could not get user data:" + graphResult.Error);
        //            }
        //            else {
        //                if (!graphResult.ResultDictionary.ContainsKey("email") || !graphResult.ResultDictionary.ContainsKey("name"))
        //                    OnServerError("Could not get user information");
        //                else {
        //                    string EMail = graphResult.ResultDictionary["email"] as string;
        //                    string FullName = graphResult.ResultDictionary["name"] as string;
        //                    var SplitedName = FullName.Split(' ');
        //                    string FirstName = SplitedName[0];
        //                    string LastName = SplitedName.Length == 1 ? "" : SplitedName[SplitedName.Length - 1];
        //                    Debug.Log("EMail: " + EMail + " FirstName: " + FirstName + " LastName: " + LastName);
        //                    StartCoroutine(MSC.SVCM.DoLogin(EMail, "", aToken.UserId, "", "", FirstName, LastName, OnServerResponse, OnServerError));
        //                }
        //            }
        //         });  
        //    }
        //    else {
        //        Debug.Log("User cancelled login");
        //        OnServerError("LOGIN_CANCELLED");
        //    }
        //});
    }


    public void TWLogin() {
        //SoomlaProfile.Login(Provider.TWITTER);
    }

    public void GOLogin() {
        //SoomlaProfile.Login(Provider.GOOGLE);
    }


    // Use this for initialization
    void Start() {
        PrepareComponents();
        Debug.Log(Application.loadedLevelName);
        if (Application.loadedLevelName == "LoginPage") {
            PrepareHandlers();
            //SoomlaProfile.Initialize();
        }
    }

	
	// Update is called once per frame
	void Update () {
	
	}


}
