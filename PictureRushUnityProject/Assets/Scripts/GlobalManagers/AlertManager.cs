﻿using UnityEngine;
using System.Collections;
using System;
using System.Linq;

public class AlertManager : MonoBehaviour {

    UITweener TAnim;
    GlobalManager GM;

    Action OnOk, OnPositive, OnNegative;

    void TurnOffTypes(Transform Root) {
        Debug.Log(GM);
        GM.SearchTransform(Root, "WDG_AT_PleaseWait").gameObject.SetActive(false); 
        GM.SearchTransform(Root, "WDG_AT_AutoClose").gameObject.SetActive(false);
        GM.SearchTransform(Root, "WDG_AT_OneButton").gameObject.SetActive(false);
        GM.SearchTransform(Root, "WDG_AT_TwoButton").gameObject.SetActive(false); 
    }

    public void ShowA(string Tittle, string Message, float AutoCloseTime = 5f,Action OnClose=null) {
        var UI2D = GameObject.Find("UI2D").transform;
        TurnOffTypes(UI2D);
        var CNL_Alert = GM.SearchTransform(UI2D, "CNL_Alert");
        var WG = GM.SearchTransform(UI2D, "WDG_AT_AutoClose");
        WG.gameObject.SetActive(true);
        WG.GetComponentsInChildren<UILabel>(true).Where(L => L.name == "LBL_Message").FirstOrDefault().text = Message;
        CNL_Alert.GetComponent<UITweener>().ResetToBeginning();
        CNL_Alert.GetComponent<UITweener>().PlayForward();
        GM.DelayAction(AutoCloseTime, () => {
            TAnim.onFinished.Clear();
            if (OnClose != null)
                TAnim.SetOnFinished(() => {
                    OnClose();
                });
            TAnim.PlayReverse();
        });
    }

    public void ShowB(string Tittle, string Message, string ButtonText = "Ok", Action OnOk = null) {
        this.OnOk = OnOk;
        var UI2D = GameObject.Find("UI2D").transform;
        TurnOffTypes(UI2D);
        var CNL_Alert = GM.SearchTransform(UI2D, "CNL_Alert");
        var WG = GM.SearchTransform(UI2D, "WDG_AT_OneButton");
        WG.gameObject.SetActive(true);
        WG.GetComponentsInChildren<UILabel>(true).Where(L=>L.name=="LBL_Message").FirstOrDefault().text = Message;
        var T = CNL_Alert.GetComponent<UITweener>();
        T.onFinished.Clear();
        Debug.Log("ShowB:T.onFinished.Count()->" + T.onFinished.Count());
        //T.ResetToBeginning();
        T.PlayForward();
    }

    public void ShowC(string Tittle, string Message, string ButtonYesText = "Yes", string ButtonNoText = "No", Action OnPositive = null, Action OnNegative = null) {
        this.OnPositive = OnPositive;
        this.OnNegative = OnNegative;
        var UI2D = GameObject.Find("UI2D").transform;
        TurnOffTypes(UI2D);
        var CNL_Alert = GM.SearchTransform(UI2D, "CNL_Alert");
        var WG = GM.SearchTransform(UI2D, "WDG_AT_TwoButton");
        WG.gameObject.SetActive(true);
        WG.GetComponentsInChildren<UILabel>(true).Where(L => L.name == "LBL_Message").FirstOrDefault().text = Message;
        CNL_Alert.GetComponent<UITweener>().ResetToBeginning();
        CNL_Alert.GetComponent<UITweener>().PlayForward();
    }

    public void OpenPleaseWait(Action OnFinish) {
        var UI2D = GameObject.Find("UI2D").transform;
        TurnOffTypes(UI2D);
        var CNL_Alert = GM.SearchTransform(UI2D, "CNL_Alert");
        var WG = GM.SearchTransform(UI2D, "WDG_AT_PleaseWait");
        WG.gameObject.SetActive(true);
        var T = CNL_Alert.GetComponent<UITweener>();
        T.onFinished.Clear();
        Debug.Log("T: " + T.onFinished.Count());
        var E = new EventDelegate(() => {
            Debug.Log("OpenPleaseWait:OnFinish");
            if (OnFinish != null)
                OnFinish();
        });
        E.oneShot = true;
        T.SetOnFinished(E);
        GM.DelayAction(1, () => {
            T.PlayForward();
        });
    }

    public void ClosePleaseWait(Action OnFinish=null) {
        var UI2D = GameObject.Find("UI2D").transform;
        var CNL_Alert = GM.SearchTransform(UI2D, "CNL_Alert");
        var WG = GM.SearchTransform(UI2D, "WDG_AT_PleaseWait");
        WG.gameObject.SetActive(true);
        var T = CNL_Alert.GetComponent<UITweener>();
        T.onFinished.Clear();
        Debug.Log("T: " + T.onFinished.Count());
        var E = new EventDelegate(() => {
            Debug.Log("ClosePleaseWait:OnFinish"); 
             if (OnFinish != null)
                 OnFinish(); 
        });
        E.oneShot = true;
        T.SetOnFinished(E);
        GM.DelayAction(1, () => {
            T.PlayReverse();
        });
    }

    public void BTN_Ok() {
        TAnim.onFinished.Clear();
        Debug.Log("OnOK  " + (OnOk != null ? "No es null" : "es null"));
        if (OnOk != null)
            TAnim.SetOnFinished(() => { OnOk(); });
        TAnim.PlayReverse();
    }
    public void BTN_Positive() {
        Debug.Log("Positive");
        TAnim.PlayReverse();
        if (OnPositive != null) OnPositive();

    }
    public void BTN_Negative() {
        TAnim.PlayReverse();
        if (OnNegative != null) OnNegative();

    }

    public void CStart() {
        GM = GameObject.Find("GlobalManager").GetComponent<GlobalManager>();
        TAnim = gameObject.GetComponentInChildren<UITweener>();
    }

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
