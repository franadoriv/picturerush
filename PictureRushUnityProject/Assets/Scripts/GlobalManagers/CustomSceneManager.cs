﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class CustomSceneManager : MonoBehaviour {

    public Texture TEX_FadeBlack;
    public Texture TEX_FadeWhite;
    public UITexture TEX_Fade;
    public MainSceneCode MSC;
    public List<Texture> LoadingImages = new List<Texture>();

    float ILBW = 0;
    GlobalManager GM;

    public bool LevelAsyncContinue = false;


    public void FadeOut(Action OnFinish = null,float FadeTime = 2F) {
		TEX_Fade = GM.SearchTransform(GameObject.Find("CNL_Fade").transform,"TEX_Fade").GetComponent<UITexture>();
        var UIT = TEX_Fade.GetComponent<UITexture>();
        iTween.ValueTo(gameObject, 0f, 1f, FadeTime / 2, (V) => {
            var C = UIT.color;
            C.a = (float)V;
            UIT.color = C;
        }, OnFinish);

    }

    public void FadeIn(Action OnFinish = null, float FadeTime = 1F) {
		TEX_Fade = GM.SearchTransform(GameObject.Find("CNL_Fade").transform,"TEX_Fade").GetComponent<UITexture>();
        var UIT = TEX_Fade.GetComponent<UITexture>();
        iTween.ValueTo(gameObject, 1f, 0f, FadeTime / 2, (V) => {
            var C = UIT.color;
            C.a = (float)V;
            UIT.color = C;
        }, OnFinish);
    }


    IEnumerator ActionDelayed(float seconds,Action Action){
        yield return new WaitForSeconds(seconds);
        Action();
    }

    public void DelayAction(float seconds, Action Action) {
        StartCoroutine(ActionDelayed(seconds, Action));
    }

    public IEnumerator BeginLevelLoad(string LevelName) {
        LevelAsyncContinue = false;
        Debug.Log("Begining of the loading of level '" + LevelName + "'");
        
        AsyncOperation async = Application.LoadLevelAsync(LevelName);
        yield return async;
        while (!LevelAsyncContinue)
            yield return new WaitForEndOfFrame();
        Debug.Log("Loading of the level '" + LevelName + "' complete!");
    }

    void SetVolume(float V) { 
    MSC.AM.BGM.volume = V;
    }

    public void ChangeScene(string SceneName,bool DoFadeIn = true,bool WhiteFade = false,bool FadeAudio=true,float FadeTime = 1.0F) {
        Debug.Log("Changing scene to: " + SceneName);

        Debug.Log("MSC: " + SceneName);
        Debug.Log("MSC.AM: " + MSC.AM);
        Debug.Log("MSC.AM.BGM: " + MSC.AM.BGM);

        if (FadeAudio)
                    iTween.ValueTo(gameObject, MSC.AM.BGM.volume, 0.0f, FadeTime/2.5f, (V) => { 
                        //Camera.main.audio.volume = (float)V;
                        MSC.AM.BGM.volume = (float)V; 
                    });
        TEX_Fade.mainTexture = WhiteFade ? TEX_FadeWhite : TEX_FadeBlack;
        TEX_Fade.alpha = 0;
        FadeOut(() => {
            DelayAction(0.25f, () => {
                GM.DelayAction(0.25f, () => {
                    Application.LoadLevel(SceneName);
                    PrepareComponents();
                    if(TEX_Fade==null)
                        TEX_Fade = GM.SearchTransform(GameObject.Find("CNL_Fade").transform, "TEX_Fade").GetComponent<UITexture>();
                    if (DoFadeIn) FadeIn(null,FadeTime/2);
                });
            });
        }, FadeTime/2);
    }

    public static void CheckSceneManager() {
        if (GameObject.Find("SceneManager") == null) {
            GameObject SM = Instantiate(Resources.Load("SceneManager")) as GameObject;
            SM.name = "SceneManager";
            //SM.GetComponentInChildren<QuestionManager>();
        }
    }

    public void PlayFlash(Action OnMiddle, Action OnFinish = null) {
        PlayFlash(0.25F, true, OnMiddle, OnFinish);
    }
    public void PlayFlash(float Duration = 0.25f, bool WhiteFade = true, Action OnMiddle = null, Action OnFinish = null) {
        TEX_Fade.mainTexture = WhiteFade ? TEX_FadeWhite : TEX_FadeBlack;
        FadeOut(() => {
            DelayAction(0.25f, () => {
                if (OnMiddle != null) OnMiddle();
                FadeIn(OnFinish, Duration / 2);
            });
        }, Duration / 2);
    
    
    }

    public void PrepareComponents() {
        GM = gameObject.AddComponent<GlobalManager>();
        MSC = GameObject.Find("MainSceneCode").GetComponent<MainSceneCode>();
        TEX_FadeBlack = Resources.Load<Texture>("BlackTexture");
        TEX_FadeWhite = Resources.Load<Texture>("WhiteTexture");
        TEX_Fade = GM.SearchTransform(GameObject.Find("CNL_Fade").transform, "TEX_Fade").GetComponent<UITexture>();
    }

    void Awake() {
        //DontDestroyOnLoad(gameObject);
    }



    void Start() {
        PrepareComponents();
    }

    void Update() {
    }

    
}
