﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class ObjectPoolManager : MonoBehaviour {

    //Managers
    MainSceneCode MSC;
    GlobalManager GM;

    public GameObject[] PooledObject;

    public GameObject GetObject(string Name,bool CaseSensitive = true) {
        return PooledObject.ToList<GameObject>().Find(O=>CaseSensitive?O.name==Name:O.name.Contains(Name));
    }

    public void LoadPool() {
        var LPO = new List<GameObject>();
        foreach (Transform T in GetComponentsInChildren<Transform>(true)) {
            LPO.Add(T.gameObject);
        }
        PooledObject = LPO.ToArray<GameObject>();
    }

	// Use this for initialization
	void Start () {
        GM = GameObject.Find("GlobalManager").GetComponent<GlobalManager>();
        MSC = GameObject.Find("MainSceneCode").GetComponent<MainSceneCode>();
        LoadPool();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
