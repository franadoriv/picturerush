﻿using UnityEngine;
using System.Collections;
using System;

public class AudioManager : MonoBehaviour {


    //public bool Debug = false;
    public GlobalManager GM;

    //Audio Objects
    public AudioSource BGM;
    public AudioSource SFX;


    public void PlaySFX(AudioClip AC) {
        if (AC != null && SFX != null)
            SFX.PlayOneShot(AC);
    }

    public void PlayBGM(AudioClip AC, bool FadeIn = true, bool FadeOut = true, float FadeTimeSeconds = 1.0f) {
        Debug.Log("Changing BGM to " + AC);
        if (AC == null) return;
        iTween.StopByName("ITW_MusicFade");
        iTween.ValueTo(gameObject, 0.5f, 0, FadeTimeSeconds, (V) => {
            BGM.volume = (float)V;
        }, () => {
            GM.DelayAction(0.25f, () => {
                BGM.Stop();
                BGM.clip = AC;
                BGM.Play();
                iTween.ValueTo(gameObject, 0, 0.5f, FadeTimeSeconds, (V) => {
                    BGM.volume = (float)V;
                }, null, "ITW_MusicFade");
            });
        }, "ITW_MusicFade");
    }

    public void SetVolume(float V) {
        BGM.volume = V;
        SFX.volume = V;
    }

    public void FadeIn(float time = 1) {
        iTween.StopByName("FadeVolume");
        SetVolume(0);
        BGM.Play();
        iTween.ValueTo(gameObject, 0, 1, time, (V) => {
            BGM.volume = (float)V;
            SFX.volume = (float)V;
        }, null, "FadeVolume");
    }

    public void BGM_FadeOut(float time = 1.0F,Action OnFinish=null) {
        iTween.StopByName("BGM_FadeVolume");
        GM.DelayFrameAction(2, () => {
            iTween.ValueTo(gameObject, BGM.volume, 0.0F, time, (V) => {
                BGM.volume = (float)V;
            }, OnFinish, "BGM_FadeVolume");       
        });
    }

    public void BGM_FadeIn(float time = 1.0F, Action OnFinish = null) {
        iTween.StopByName("BGM_FadeVolume");
        GM.DelayFrameAction(2, () => {
            BGM.Play();
            iTween.ValueTo(gameObject, BGM.volume, 0.5F, time, (V) => {
                BGM.volume = (float)V;
            }, OnFinish, "BGM_FadeVolume");
        });
    }


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
