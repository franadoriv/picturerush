﻿using UnityEngine;
using System.Collections;

public class InterfaceManager : MonoBehaviour {

    //Managers
    MainSceneCode MSC;
    GameObject PNL_Score;

    public void AddScore(int Points) {
        MSC.UM.CurrentPoints += Points;
        PNL_Score.GetComponentInChildren<UILabel>().text = "Score: " + MSC.UM.CurrentPoints;
    }

	// Use this for initialization
	void Start () {
        MSC = GameObject.Find("MainSceneCode").GetComponent<MainSceneCode>();
        PNL_Score = GameObject.Find("PNL_Score");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
