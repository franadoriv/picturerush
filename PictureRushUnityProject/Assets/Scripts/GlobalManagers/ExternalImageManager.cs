﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using System.IO;
public class ExternalImageManager : MonoBehaviour {

    
    public Texture[] UserTextures;
    public Texture[] DefaultTextures;
    public GameObject OBJ_PictureItem;
    public ServiceManager SVCM;
    public AlertManager ALTM;
    public UIPopupList POPL;
    public FileBrowserManager FBM;
    public MainSceneCode MSC;

    public objPictureGroup CurrentPictureGroup ;

    void PrepareComponents() {
        MSC = GameObject.Find("MainSceneCode").GetComponent<MainSceneCode>();
        SVCM = GameObject.Find("ServiceManager").GetComponent<ServiceManager>();
        ALTM = GameObject.Find("CNL_Alert").GetComponent<AlertManager>();
       
        FBM = GameObject.Find("FileBrowserManager").GetComponent<FileBrowserManager>();
    }

    public Texture GetTexture(string TextureName) {
        foreach (Texture T in DefaultTextures)
            if (T.name == TextureName) return T;
        foreach(Texture T in UserTextures)
            if (T.name == TextureName) return T;
        return null;
    }

    public Texture GetRandomTexture(bool UseOnlyDefaultTextures = false) {
        if (UseOnlyDefaultTextures) {
            int I = Random.Range(0, DefaultTextures.Count() - 1);
            return DefaultTextures[I];
        }
        else {
            int IA = Random.Range(0, DefaultTextures.Count() - 1);
            int IB = Random.Range(0, UserTextures.Count() - 1);
            int C = UserTextures.Count()>0?Random.Range(0,4):0;
            //Debug.Log("Showing texture: " + C + " UserTextures.Count: " + UserTextures.Count());
            return C >= 2 ? UserTextures[IB] : DefaultTextures[IA];
        }
    }

    public void LoadPicTypeCombo() {
        Debug.Log("1: " + MSC.GM);
        Debug.Log("2: " + MSC.GM.SearchTransform(MSC.IM.transform, "LBL_PleaseWaitServerImages"));
        Debug.Log("3: " + MSC.IM);
        var LBL_PleaseWaitServerImages = MSC.GM.SearchTransform(MSC.IM.transform, "LBL_PleaseWaitServerImages").gameObject;
        //if (LBL_PleaseWaitServerImages == null) throw new Exception("OMG LBL_PleaseWaitServerImages is NULL!!!!");
        Debug.Log("Loading Pic Type Combo");
        POPL = GameObject.Find("CMB_PictureGroup").GetComponent<UIPopupList>();
        Debug.Log(SVCM);
        Debug.Log(MSC.ACTM);
        Debug.Log(MSC.ACTM.UserID);
        LBL_PleaseWaitServerImages.gameObject.SetActive(true);
        StartCoroutine(SVCM.GetPictureGroups(MSC.ACTM.UserID, (GL) => { 
            POPL.Clear();
            foreach (var Group in GL) {
                POPL.AddItem(Group.PictureGroupName, Group);
            }
            POPL.onChange.Add(new EventDelegate (()=>{
                LoadPicsMenu((objPictureGroup)POPL.data);
            }));
            //This line force the load of the first type image
            CurrentPictureGroup = (objPictureGroup)POPL.itemData[0];
            if(CurrentPictureGroup!=null)
                LoadPicsMenu((objPictureGroup)POPL.itemData.Where(PG => ((objPictureGroup)PG).PictureGroupID == CurrentPictureGroup.PictureGroupID).FirstOrDefault());
            LBL_PleaseWaitServerImages.SetActive(false);
        }, (e) => {
            LBL_PleaseWaitServerImages.SetActive(false);
            ALTM.ShowB("Error", e);
        }));
    }
   
    public void LoadPicsMenu(objPictureGroup PictureGroup) {
        Debug.Log("Laoding Picture Group: " + PictureGroup.PictureGroupID);
        CurrentPictureGroup = PictureGroup;
        MSC.GM.SearchTransform(MSC.transform, "TEX_UploadPicture").gameObject.SetActive(true);
        var SCL_Pictures = GameObject.Find("SCL_Pictures");
        foreach (Transform T in SCL_Pictures.GetComponentsInChildren<Transform>(true).Where(C=>C.name!=SCL_Pictures.name)) {
            Destroy(T.gameObject);
        }
        Vector2 LastPos = new Vector2(0,0);
        foreach (var P in PictureGroup.Pictures) {
            var NPI = ((GameObject)Instantiate(OBJ_PictureItem)).GetComponent<PictureItemHelper>();
            NPI.LoadPicture(P);
            NPI.transform.parent = SCL_Pictures.transform;
            NPI.transform.localScale = new Vector3(1,1,1);
            NPI.transform.localPosition = LastPos;
            var T = NPI.GetComponent<UISprite>();
            LastPos.x += T.width + 10;
        }
        SCL_Pictures.GetComponent<UIScrollView>().UpdateScrollbars() ;
    }

    public void UploadPicture() {
        FBM.Show((PathStr) => {
            ALTM.ShowC("", "Are you sure you want upload this file?", "Yes", "No", () => {
                ALTM.OpenPleaseWait(() => {
                    Debug.Log("Uploading picture on -> Path.GetFileName(PathStr):" + Path.GetFileName(PathStr) + " PathStr: " + PathStr);
                    StartCoroutine(SVCM.UploadPicture(MSC.ACTM.UserID, CurrentPictureGroup.PictureGroupID, Path.GetFileName(PathStr), PathStr, () => {
                        ALTM.ClosePleaseWait(() => {
                            ALTM.ShowB("", "Image uploaded!", "Ok");
                            LoadPicTypeCombo();
                        });
                    }, (ErrMess) => {
                        ALTM.ClosePleaseWait(() => {
                            ALTM.ShowB("Error", ErrMess);
                        });
                    }));
                });
            });
        
        });
    }

    public void DeletePicture(int PictureID) {
        ALTM.OpenPleaseWait(() => {
            StartCoroutine(SVCM.DeletePicture(PictureID, () => {
                ALTM.ClosePleaseWait(() => {
                    ALTM.ShowB("", "Image deleted", "Ok");
                    LoadPicTypeCombo();
                });
            }, (ErrMess) => {
                ALTM.ClosePleaseWait(() => {
                    ALTM.ShowB("Error", ErrMess);
                });
            }));
        });
    }


	// Use this for initialization
	void Start () {
        PrepareComponents();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
