﻿using UnityEngine;
using System.Collections;
using System;
using System.Linq;
using System.Collections.Generic;

public class MIN_MainMenu : MainInputManager {

    //******************* Interfaces **************

   
    //******************* On Login ****************/
  
    public void BTN_FacebookLogin() {
        MSC.GM.SearchTransform(MSC.transform, "TEX_MyPictures").GetComponent<BoxCollider>().enabled = true;
            var T = MSC.GM.SearchTransform(transform, "PNL_Login").GetComponent<UITweener>();
            T.onFinished.Clear();
            T.AddOnFinished(new EventDelegate(() => {
                MSC.ALTM.OpenPleaseWait(() => {
                    T.onFinished.Clear();
                    MSC.ACTM.FBLogin();
                });
            }));
            T.PlayReverse();
    }

    public void BTN_TwitterLogin() {
        MSC.GM.SearchTransform(MSC.transform, "TEX_MyPictures").GetComponent<BoxCollider>().enabled = true;
        var T = MSC.GM.SearchTransform(transform, "PNL_Login").GetComponent<UITweener>();
        T.onFinished.Clear();
        T.AddOnFinished(new EventDelegate(() => {
            MSC.ALTM.OpenPleaseWait(() => {
                T.onFinished.Clear();
                MSC.ACTM.TWLogin();
            });
        }));
        T.PlayReverse();
    }

    public void BTN_GoogleLogin() {
        MSC.GM.SearchTransform(MSC.transform, "TEX_MyPictures").GetComponent<BoxCollider>().enabled = true;
        var T = MSC.GM.SearchTransform(transform, "PNL_Login").GetComponent<UITweener>();
        T.onFinished.Clear();
        T.AddOnFinished(new EventDelegate(() => {
            MSC.ALTM.OpenPleaseWait(() => {
                T.onFinished.Clear();
                MSC.ACTM.GOLogin();
            });
        }));
        T.PlayReverse();
    }

    public void BTN_DefaultLogin() {
        string EMail = GameObject.Find("TXT_EMail").GetComponent<UIInput>().value;
        string Password = GameObject.Find("TXT_Password").GetComponent<UIInput>().value;
        Debug.Log(MSC);
        Debug.Log(MSC.GM);
        Debug.Log(MSC.GM.SearchTransform(MSC.transform, "TEX_MyPictures"));
        MSC.GM.SearchTransform(MSC.transform, "TEX_MyPictures").GetComponent<BoxCollider>().enabled = true;
        if (string.IsNullOrEmpty(EMail) || string.IsNullOrEmpty(Password)) {
            MSC.ALTM.ShowB("", "Please fill both fields");
        }
        else {
            Debug.Log("Login with " + EMail + " and " + Password);
            MSC.MIM.ClosePanelByName("PNL_Login", () => {
                MSC.ALTM.OpenPleaseWait(() => {
                    StartCoroutine(MSC.SVCM.DoLogin(EMail, Password, "", "", "", "", "", (UserID) => {
                        MSC.CM.IsConnected = true;
                        MSC.CM.PassedThroughLogin = true;
                        MSC.ACTM.UserID = UserID;
                        Action F = () => {
                            MSC.ALTM.ClosePleaseWait(() => {
                                MSC.MIM.OpenPanelByName("PNL_MainPanel");
                            });
                        };
                        MSC.SVCM.DeviceRegister((R) => { F(); }, (E) => { F(); });
                    },
                    (ErrMess) => {
                        MSC.ALTM.ClosePleaseWait(() => {
                            MSC.GM.DelayFrameAction(1, () => {
                                MSC.ALTM.ShowB("Error", ErrMess, "OK", () => {
                                    MSC.GM.DelayFrameAction(1, () => {
                                        MSC.MIM.OpenPanelByName("PNL_Login");
                                    });
                                });
                            });
                        });

                    }));
                });
            });
        }
    }

    public void BTN_PlayOffLine() {
        MSC.MIM.ChangeToPanelByName("PNL_MainPanel");
        MSC.CM.IsConnected = false;
        MSC.CM.PassedThroughLogin = true;
    }

    public void BTN_UploadPicture() {
        MSC.EIM.UploadPicture();
    }

    public void BTN_MyPictures() {
        if (MSC.CM.IsConnected)
            MSC.MIM.ChangeToPanelByName("PNL_MyPictures");
        else {
            MSC.MIM.ClosePanelByName("PNL_MainPanel", () => { 
                MSC.ALTM.ShowB("Error", "You are not connected", "OK", () => {
                    MSC.MIM.OpenPanelByName("PNL_MainPanel");
                });
            });
        }

    }

    public void BTN_Configuration() {
        MSC.MIM.ChangeToPanelByName("PNL_Configuration");
    }

    public void BTN_GoBack() {
        MSC.MIM.ChangeToPanelByName("PNL_Login");
        MSC.CM.IsConnected = false;
    }

    public void CHK_SelectImage(PictureItemHelper PIH) { 
        
    
    }

    public void BTN_DeleteImage(GameObject Picture) {
        MSC.ALTM.ShowC("", "You want to delete this image?", "Yes", "No", () => {
            int PictureID = Picture.GetComponent<PictureItemHelper>().PictureID;
            Debug.Log("Deleting picture " + PictureID);
            MSC.EIM.DeletePicture(PictureID);
        });
    }

    public void BTN_OnStartGame() {
        MSC.MIM.ChangeToPanelByName("PNL_GameModes");
    }

    public void BTN_GameModeSelect(GameObject GameModeButton) {
        if (GameModeButton.name == "BTN_ClasicMode")
            //MSC.CM.GameOptions = new ConfigManager.Options(ConfigManager.Options.GameModes.Classic,"",MSC.CM.CurrentGameDifficulty);
            MSC.CM.CurrentGameMode = ConfigManager.Options.GameModes.Classic;
        else if (GameModeButton.name == "BTN_ZenMode")
            //MSC.CM.GameOptions = new ConfigManager.Options(ConfigManager.Options.GameModes.Zen, "", MSC.CM.CurrentGameDifficulty);
            MSC.CM.CurrentGameMode = ConfigManager.Options.GameModes.Zen;
        else if (GameModeButton.name == "BTN_RelayMode")
            //MSC.CM.GameOptions = new ConfigManager.Options(ConfigManager.Options.GameModes.Relay, "", MSC.CM.CurrentGameDifficulty);
            MSC.CM.CurrentGameMode = ConfigManager.Options.GameModes.Relay;
        else if (GameModeButton.name == "BTN_ArcadeMode")
            //MSC.CM.GameOptions = new ConfigManager.Options(ConfigManager.Options.GameModes.Arcade, "", MSC.CM.CurrentGameDifficulty);
            MSC.CM.CurrentGameMode = ConfigManager.Options.GameModes.Arcade;
        else if (GameModeButton.name == "BTN_RushMode")
            //MSC.CM.GameOptions = new ConfigManager.Options(ConfigManager.Options.GameModes.Rush, "", MSC.CM.CurrentGameDifficulty);
            MSC.CM.CurrentGameMode = ConfigManager.Options.GameModes.Rush;
        switch (MSC.CM.CurrentGameMode) {
            case ConfigManager.Options.GameModes.Classic:
                MSC.MIM.ChangeToPanelByName("PNL_ChooseMaxTiles");
                break;
            case ConfigManager.Options.GameModes.Zen:
            case ConfigManager.Options.GameModes.Relay:
                MSC.MIM.ChangeToPanelByName("PNL_ChooseTime");
                break;
            default:
                MSC.MIM.ChangeToPanelByName("PNL_ChooseDifficulty");    
                break;
        
        }
        
        

    }

    void LoadGame() {
        Debug.Log("Loading game " + MSC.CM.GameOptions.Difficulty);
        if (MSC.CM.IsConnected) {
            Debug.Log("Connection detected, getting custom pictures");
            MSC.ALTM.OpenPleaseWait(() => {
                Debug.Log("Calling webservice to obtain pictures");
                StartCoroutine(MSC.SVCM.GetUserPictures(MSC.ACTM.UserID, true, true, (Pictures) => {
                    Debug.Log("Custom pictures found: " + Pictures.Count());
                    var TS = new List<Texture2D>();
                    foreach (var P in Pictures) { 
                        byte[] b64_bytes = System.Convert.FromBase64String(P.Base64Data);
                        var tex = new Texture2D(1,1);
                        tex.LoadImage(b64_bytes);
                        TS.Add(tex); 
                    }
                    MSC.EIM.UserTextures = TS.ToArray();
                    MSC.SM.ChangeScene("MainGame3");
                }, (ErrMess) => {
                    MSC.ALTM.ClosePleaseWait(() => {
                        MSC.GM.DelayFrameAction(1, () => {
                            MSC.ALTM.ShowB("Error", ErrMess, "OK", () => {
                                MSC.GM.DelayFrameAction(1, () => {
                                    MSC.MIM.ChangeToPanelByName("PNL_GameModes");
                                });
                            });
                        });
                    });
                }));
            });
        }
        else {
            Debug.Log("No connection detected");
            MSC.SM.ChangeScene("MainGame3");
        }
    }

    public void BTN_ChooseSpeed(GameObject SpeedTypeButton) {
        int Speed = Convert.ToInt32(SpeedTypeButton.name.Split('_')[2]);
        Debug.Log("Speed selected: " + Speed);
        MSC.CM.TilesSpeed = Speed;
        MSC.MIM.ChangeToPanelByName("PNL_ChooseDifficulty");
    }

    public void BTN_ChooseTime(GameObject TimeTypeButton) {
        int Seconds = Convert.ToInt32(TimeTypeButton.name.Split('_')[2]);
        Debug.Log("Seconds selected: " + Seconds);
        MSC.CM.CurrentSeconds = Seconds;
        MSC.MIM.ChangeToPanelByName("PNL_ChooseDifficulty");
    }

    public void BTN_ChooseMaxTiles(GameObject MaxTilesButton) {
        int MaxTiles = Convert.ToInt32(MaxTilesButton.name.Split('_')[2]);
        MSC.CM.MaxCorrectTiles = MaxTiles;
        MSC.CM.CurrentScoreGoal = MaxTiles;
        MSC.MIM.ChangeToPanelByName("PNL_ChooseDifficulty");
    }

    public void BTN_ChooseDifficulty(GameObject DifficultyButton) {
        if (DifficultyButton.name.Contains("Easy")) MSC.CM.CurrentGameDifficulty = ConfigManager.Options.GameDifficulty.Easy;
        if (DifficultyButton.name.Contains("Normal")) MSC.CM.CurrentGameDifficulty = ConfigManager.Options.GameDifficulty.Normal;
        if (DifficultyButton.name.Contains("Hard")) MSC.CM.CurrentGameDifficulty = ConfigManager.Options.GameDifficulty.Hard;
        MSC.CM.GameOptions = new ConfigManager.Options(MSC.CM.CurrentGameMode, "", MSC.CM.CurrentGameDifficulty);
        MSC.CM.GameOptions.ScoreGoal = MSC.CM.CurrentScoreGoal;
        switch (MSC.CM.GameOptions.GameMode) {
            case ConfigManager.Options.GameModes.Zen:
            case ConfigManager.Options.GameModes.Relay:
                TimeSpan.TryParse("0:0:" + MSC.CM.CurrentSeconds, out MSC.CM.GameOptions.TimerStart);
                Debug.Log("Now the time is: " + MSC.CM.GameOptions.TimerStart);
                break;
            default:
                MSC.CM.TimeOutSeconds = MSC.CM.CurrentSeconds;
                break;
        }
        MSC.MIM.ClosePanel(GameObject.Find("PNL_ChooseDifficulty"), () => {
            LoadGame();
        });
    }

    public void BTN_OnPicItemOneClick(GameObject PictureItem) {
        var CHK = MSC.GM.SearchTransform(PictureItem.transform, "CHK_UseImage").GetComponent<UIToggle>();
        CHK.value = !CHK.value;
    }

    public void BTN_OnPicItemDoubleClick() {

        Debug.Log("Double Click!");
    }


    public void BTN_ChangeToPanel(GameObject To) {
        GameObject Actual;
        var ActualT = MSC.GM.SearchTransforms(MSC.IM.transform, "PNL_", false, false).ToList().FirstOrDefault();
        Actual = ActualT != null ? ActualT.gameObject : null;
        MSC.MIM.ChangeToPanel(Actual, To, null);
    }

    public void BTN_BackFromUserPics(GameObject PicsContainer) {
        var PS = new List<ServiceManager.PictureStatus>();
        foreach (Transform P in MSC.GM.SearchTransforms(PicsContainer.transform, "CHK_UseImage", false)) {
            PS.Add(new ServiceManager.PictureStatus(P.transform.parent.GetComponent<PictureItemHelper>().PictureID, P.GetComponent<UIToggle>().value));
            DestroyObject(P.transform.parent.gameObject);
        }
        MSC.MIM.ClosePanel(MSC.GM.SearchTransform(transform, "PNL_MyPictures").gameObject, () => {
            MSC.ALTM.OpenPleaseWait(() => {
                Debug.Log("OpenPleaseWait");
                StartCoroutine(MSC.SVCM.SetPicturesStatus(PS, () => {
                    Debug.Log("Finish UpdatePicturesStatus");
                    MSC.ALTM.ClosePleaseWait(() => {
                        Debug.Log("ClosePleaseWait on finish");
                        MSC.MIM.OpenPanelByName("PNL_MainPanel");
                    });
                }, (ErrMess) => { }));
            });
        });
    }

    public void OnComboOpen(GameObject OBJ_Combo) {
        var DDL = MSC.GM.SearchTransform(OBJ_Combo.transform.parent, "Drop-down List");
        MSC.GM.DelayFrameAction(1, () => {
            var LP = DDL.transform.localPosition;
            LP.x = 0;
            DDL.transform.localPosition = LP;
        });
    }


    public void OnLoadMyImagesPanel() {
        if(MSC.CM.IsConnected)
            MSC.GM.DelayAction(1, () => {
                MSC.EIM.LoadPicTypeCombo(); 
            });
    }

    public void OnTilesSpeedChange(float SLD_TilesSpeed) {
        MSC.CM.TilesSpeed = SLD_TilesSpeed;
    }


    public void OnMusicVolumeChange(float SLD_MusicVolume) {
        MSC.AM.BGM.volume = SLD_MusicVolume;
    }

    public void OnEffectsVolumeChange(float SLD_EffectsVolume) {
        MSC.AM.SFX.volume = SLD_EffectsVolume;
    }

    public void OnDifficultyChange(string DifficultyLevel) {
        Debug.Log("DifficultyLevel: " + DifficultyLevel);
        MSC.CM.CurrentGameDifficulty = (ConfigManager.Options.GameDifficulty)Enum.Parse(typeof(ConfigManager.Options.GameDifficulty), DifficultyLevel, true);
    }

    //**************************************************



   
	// Use this for initialization
	void Start () {
        MSC = GameObject.Find("MainSceneCode").GetComponent<MainSceneCode>();
        Debug.Log(MSC);
	}
	
	// Update is called once per frame
	void Update () {
        
	}
}
