﻿using UnityEngine;
using System.Collections;
using System;

public class objPicture {
        public int PictureID;
        public string PictureName;
        public string Base64Data;
        public DateTime DateStored;
        public bool UseThisPicture=false;
        public objPicture() { }

        public objPicture(int PictureID, string PictureName, DateTime DateStored, bool UseThisPicture,string Base64Data = "") {
            this.PictureID = PictureID;
            this.PictureName = PictureName;
            this.DateStored = DateStored;
            this.UseThisPicture = UseThisPicture;
            this.Base64Data = Base64Data;
        }
}
