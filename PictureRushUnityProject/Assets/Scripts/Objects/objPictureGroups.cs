﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class objPictureGroup {

        public int PictureGroupID;
        public string PictureGroupName;
        public List<objPicture> Pictures = new List<objPicture>(); 
        public objPictureGroup() { }

        public objPictureGroup(int PictureGroupID, string PictureGroupName) {
            this.PictureGroupID = PictureGroupID;
            this.PictureGroupName = PictureGroupName;
        }
	
}
