﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class ScreenShoot : MonoBehaviour {

    public GameObject CharContent;
    public GameObject CamerasContent;

    IEnumerator StartCapture() {
        var Chars = CharContent.GetComponentsInChildren<Transform>(true).Where(T => T.name.Contains("SimpleCitizens_"));
        var Cameras = CamerasContent.GetComponentsInChildren<Camera>(true);
        Debug.Log("Se encontraron " + Chars.Count() + " characters");
        int N = 0;
        foreach (Transform Char in Chars) {
            float R = Random.Range(0.0F, 3.0F);
            Debug.Log("Tomando foto: " + N + " R: " + R);
            foreach (Camera C in Cameras) C.gameObject.SetActive(false);
            if (R >= 2.0) Cameras[0].gameObject.SetActive(true);
            else if (R >= 1.0) Cameras[1].gameObject.SetActive(true);
            else if (R >= 0.0) Cameras[2].gameObject.SetActive(true);
            yield return new WaitForEndOfFrame();
            Char.gameObject.SetActive(true);
            yield return new WaitForEndOfFrame();
            Application.CaptureScreenshot("SimpleCitizens_"+N+".png");
            yield return new WaitForEndOfFrame();
            Char.gameObject.SetActive(false);
            N++;
            yield return new WaitForSeconds(0.5F);
        }
        Cameras[0].gameObject.SetActive(true);
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.C)) {
            Debug.Log("Iniciando");
            StartCoroutine(StartCapture());
        }
	}
}
